module numerovcooley
    use iso_fortran_env, only: stderr => error_unit, int32, real64

    implicit none

    private
    public :: numerov_cooley, numerov_cooley_single, numerov_search
contains
    subroutine numerov_search(psi, E, V, dr, N, lower, upper, target_nodes, max_iters, cooley_convergence_iters)
        integer(int32), intent(in) :: N, target_nodes
        real(real64), intent(out) :: psi(N), E
        real(real64), intent(in) :: dr, V(N), lower, upper
        integer(int32), intent(in), optional :: max_iters, cooley_convergence_iters

        integer(int32) :: max_iters_inner, nodes, i, j
        real(real64) :: lower_inner, upper_inner, prev_E, prev_psi

        ! Changing `intent(in)` parameters is not allowed, so this is required instead...
        if (present(max_iters)) then
            max_iters_inner = max_iters
        else
            max_iters_inner = 10
        end if
        lower_inner = lower
        upper_inner = upper

        do i = 1, max_iters_inner
            E = lower_inner + (upper_inner - lower_inner) / 2.0_real64
            prev_E = E
            call numerov_cooley_single(psi, E, V, dr, N)

            nodes = 0
            prev_psi = psi(1)
            do j = 2, N
                ! NOTE: Assume none of the points are ever zero.
                if (psi(j) * prev_psi < 0.0_real64) then
                    nodes = nodes + 1
                end if
                prev_psi = psi(j)
            end do

            if (mod(i - 1, max_iters_inner / 10) == 0) then
                write (stderr, *) &
                    "Bisection Iteration", i, &
                    "lower", lower_inner, &
                    "upper", upper_inner, &
                    "E", prev_E, &
                    "E + dE", E, &
                    "nodes", nodes
            end if

            if (nodes > target_nodes) then
                upper_inner = prev_E
            else if (nodes < target_nodes) then
                lower_inner = prev_E
            else
                exit
            end if

            if (i == max_iters_inner) then
                error stop "Numerov node counting bisection failed."
            end if
        end do

        call numerov_cooley(psi, E, V, dr, N, cooley_convergence_iters)
    end subroutine

    subroutine numerov_cooley(psi, E, V, dr, N, max_iters)
        integer(int32), intent(in) :: N
        real(real64), intent(out) :: psi(N)
        real(real64), intent(inout) :: E
        real(real64), intent(in) :: dr, V(N)
        integer(int32), intent(in), optional :: max_iters

        integer(int32) :: i, max_iters_inner
        real(real64) :: prev_E, dE

        ! Changing `intent(in)` parameters is not allowed, so this is required instead...
        if (present(max_iters)) then
            max_iters_inner = max_iters
        else
            max_iters_inner = 10000
        end if

        do i = 1, max_iters_inner
            prev_E = E
            call numerov_cooley_single(psi, E, V, dr, N)
            dE = abs(prev_E - E)

            if (mod(i - 1, max_iters_inner / 10) == 0) then
                write (stderr, *) &
                    "Numerov-Cooley Iteration", i, &
                    "E", prev_E, &
                    "dE", dE, &
                    "E + dE", E
            end if

            if (dE < 1.0e-6_real64) then
                exit
            end if
        end do
    end subroutine

    subroutine numerov_cooley_single(psi, E, V, dr, N)
        integer(int32), intent(in) :: N
        real(real64), intent(out) :: psi(N)
        real(real64), intent(inout) :: E
        real(real64), intent(in) :: V(N), dr

        real(real64) :: P(N), Y(N), k, dE
        integer(int32) :: j, m

        P = -2.0_real64 * (V - E)
        k = dr**2 / 12.0_real64

        psi = 0.0_real64
        psi(2) = 1.0e-6_real64
        psi(N - 1) = 1.0e-6_real64

        m = N - 1
        do j = 2, N-2
            psi(j + 1) = ( &
                2.0_real64 * psi(j) * (1.0_real64 - 5.0_real64 * k * P(j)) - &
                psi(j - 1) * (1.0_real64 + k * P(j - 1)) &
            ) / (1.0_real64 + k * P(j - 1))

            if (psi(j) < psi(j - 1)) then
                m = j + 1
                exit
            end if
        end do

        psi(:m-1) = psi(:m-1) / psi(m)

        do j = N-1, m+1, -1
            psi(j - 1) = ( &
                2.0_real64 * psi(j) * (1.0_real64 - 5.0_real64 * k * P(j)) - &
                psi(j + 1) * (1.0_real64 + k * P(j + 1)) &
            ) / (1.0_real64 + k * P(j + 1))
        end do

        psi(m:) = psi(m:) / psi(m)

        ! Cooley correction.
        Y = (1.0_real64 - 2.0_real64 * k * (V - E)) * psi
        dE = &
            (psi(m) / sum(abs(psi))**2) * &
            ( &
                -0.5_real64 * (Y(m + 1) - 2 * Y(m) + Y(m - 1)) / dr**2 + &
                (V(m) - E) * psi(m) &
            )

        E = E + dE
    end subroutine
end module numerovcooley