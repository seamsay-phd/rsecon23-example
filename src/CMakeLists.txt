set(
    MODULES
    cli.f90
    hartree.f90
    integration.f90
    numerovcooley.f90
    CACHE INTERNAL ""
)

add_library(hartree_lib ${MODULES})