module integration
    use iso_fortran_env, only: int32, real64

    implicit none

    public :: normalise, simpsons
contains
    subroutine normalise(psi, dr, N)
        integer(int32), intent(in) :: N
        real(real64), intent(inout) :: psi(N)
        real(real64), intent(in) :: dr

        real(real64) :: area

        area = simpsons(psi**2, dr, N)
        psi = psi / sqrt(area)
    end subroutine

    function simpsons(array, dr, N)
        integer(int32), intent(in) :: N
        real(real64), intent(in) :: dr, array(N)
        real(real64) :: simpsons

        logical :: is_odd
        integer(int32) :: N_inner

        is_odd = mod(N, 2) /= 0
        if (is_odd) then
            N_inner = N - 1
        else
            N_inner = N
        end if

        simpsons = array(1) + 4*sum(array(2:N_inner-1:2)) + 2*sum(array(3:N_inner-2:2)) + array(N_inner)
        simpsons = simpsons * dr / 3.0_real64

        if (is_odd) then
            ! As a hack, add a single trapezoid rule evaluation onto the end if the array length is odd.
            simpsons = simpsons + (array(N - 1) + array(N)) * dr / 2.0_real64
        end if
    end function
end module integration