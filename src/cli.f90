module cli
    use iso_fortran_env, only: stderr => error_unit, int32, real64

    implicit none

    integer(int32), parameter :: electrons = 3

    public :: electrons, read_parameters
contains
    subroutine read_parameters(dr, r_max, Z, E_adj)
        real(real64), intent(out) :: dr, r_max, E_adj(electrons)
        integer(int32), intent(out) :: Z

        integer(int32) :: i, n
        character(len=:), allocatable :: arg

        Z = 3
        E_adj = [0.0_real64, 0.0_real64, 0.875_real64]
        r_max = 20.0_real64
        dr = 1.0e-3_real64

        n = command_argument_count()
        i = 0
        do while (i < n)
            i = i + 1

            arg = get_argument(i)

            select case (arg)
            case ("-h", "--help")
                deallocate (arg)
                arg = get_argument(0)
                print *, &
                    "usage: ", arg, &
                    " [-r R-MAX]", &
                    " [-Z CHARGE]", &
                    " [-E ELECTRON-1-ENERGY-ADJUST ELECTRON-2-ENERGY-ADJUST ELECTRON-3-ENERGY-ADJUST]"
                stop
            case ("-Z", "--charge")
                i = i + 1
                if (i > n) then
                    error stop "--charge (-Z) requires an argument"
                end if
                Z = get_integer_argument(i)
            case ("-r", "--r-max")
                i = i + 1
                if (i > n) then
                    error stop "--r-max (-r) requires an argument"
                end if
                r_max = get_real_argument(i)
            case ("-d", "--r-step")
                i = i + 1
                if (i > n) then
                    error stop "--r-step (-d) requires an argument"
                end if
                dr = get_real_argument(i)
            case ("-E", "--energy-adjust")
                if (i + 3 > n) then
                    error stop "--energy-adjust (-E) requires 3 arguments"
                end if

                i = i + 1
                E_adj(1) = get_real_argument(i)
                i = i + 1
                E_adj(2) = get_real_argument(i)
                i = i + 1
                E_adj(3) = get_real_argument(i)
            case default
                write (stderr, *) "Unknown argument: ", arg
                error stop
            end select

            deallocate (arg)
        end do
    end subroutine

    function get_integer_argument(i) result(argument)
        integer(int32), intent(in) :: i
        integer(int32) :: argument

        character(len=:), allocatable :: text
        text = get_argument(i)
        read (text, *) argument
    end function

    function get_real_argument(i) result(argument)
        integer(int32), intent(in) :: i
        real(real64) :: argument

        character(len=:), allocatable :: text
        text = get_argument(i)
        read (text, *) argument
    end function

    function get_argument(i) result(argument)
        integer(int32), intent(in) :: i
        character(len=:), allocatable :: argument

        integer(int32) :: length

        call get_command_argument(i, length = length)
        allocate (character(len=length) :: argument)
        call get_command_argument(i, value = argument)
    end function
end module cli