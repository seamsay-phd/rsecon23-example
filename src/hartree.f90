module hartree
    use iso_fortran_env, only: stderr => error_unit, int32, real64
    use cli, only: electrons
    use integration, only: normalise, simpsons
    use numerovcooley, only: numerov_search, numerov_cooley

    implicit none

    private
    public :: hartree_method, lithium
contains
    subroutine lithium(psi, E, dr, N)
        integer(int32), intent(in) :: N
        real(real64), intent(out) :: psi(N, electrons), E(electrons)
        real(real64), intent(in) :: dr

        call hartree_method(psi, E, dr, 3, N, [0.0_real64, 0.0_real64, 0.875_real64])
    end subroutine

    subroutine hartree_method(psi, E, dr, Z, N, E_adj)
        integer(int32), intent(in) :: N, Z
        real(real64), intent(out) :: psi(N, electrons), E(electrons)
        real(real64), intent(in) :: dr, E_adj(electrons)

        integer(int32), parameter :: hartree_iterations = 5

        real(real64) :: r(N), r_min, V(N), Qenc(N), rho(N)
        integer(int32) :: i, j, k

        r_min = 1.0e-5_real64
        r = [(r_min + dr*i, i=0,N-1)]
        psi = 0.0_real64
        V = -Z/r

        call numerov_search(psi(:, 1), E(1), V, dr, N, -10.0_real64, -1.0_real64, 0)
        call numerov_search(psi(:, 2), E(2), V, dr, N, -10.0_real64, -1.0_real64, 0)
        call numerov_search(psi(:, 3), E(3), V, dr, N, -10.0_real64, -1.0_real64, 1)
        E = E + E_adj

        do i = 1,electrons
            call normalise(psi(:, i), dr, N)
        end do

        do i = 1, hartree_iterations
            do j = 1, electrons
                rho = 0.0_real64
                do k = 1, electrons
                    if (j /= k) rho = rho + abs(psi(:, k))**2
                end do

                Qenc = 0.0_real64
                do k = 2, N
                    Qenc(k) = simpsons(rho(:k), dr, k)
                end do

                V = -Z/r + Qenc/r

                call numerov_cooley(psi(:, j), E(j), V, dr, N)
                call normalise(psi(:, j), dr, N)

                write (stderr, *) &
                    "Hartree Iteration", i, &
                    "Electron", j, &
                    "E", E(j)
            end do
        end do
    end subroutine
end module hartree
