git checkout main

if (git log -1 --format=format:%s) != 'Add a script to generate the README.' {
    error make --unspanned {msg: "New commit made, this script's use of `commit --amend` will no longer work."}
}

let readme = (mktemp)
let graphs = (mktemp -d | str trim)
let gitlab = "https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/"

print $'README: ($readme)'
print $'Graphs: ($graphs)'

let hashes = (git log --format=format:'%h %H' | lines | parse '{short} {long}' | last 2)
let initial = ($hashes | last)
let globalised = ($hashes | first)

git checkout $initial.long
d2 --layout elk call-graph.d2 $"($graphs)/($initial.short).svg"
git checkout $globalised.long
d2 --layout elk call-graph.d2 $"($graphs)/($globalised.short).svg"
git checkout main

$"# RSECon23 Legacy Refactoring Example

This is a git repository designed to act as an example of the process outlined in my RSECon23 talk \"A Global Revolt\". You can find a more thoroughly explained version of this process in the \(currently in-progress) paper [found here]\(https://gitlab.com/seamsay-phd/refactor).

This repository uses the Fortran 90 Lithium example from Chapter 13.4 of Computational Quantum Mechanics by Izaac & Wang to provide a small but realistic example program. A one-to-one implementation \(almost) of this code is available in [the first commit, ($initial.short)]\(($gitlab)($initial.long)) \(to their credit this version is very similar to the final version).

![Call graph of the code on which this example is based.]\(call-graphs/($initial.short).svg)

The next commit \([($globalised.short)]\(($gitlab)($globalised.long))) contains a \"globalised\" version of that code, where a set of variables have been changed to global variables accessed by all parts of the code. While in a toy example like this readability and maintainability are not hugely affected, this work is based on [RMT]\(https://gitlab.com/Uk-amor/RMT/rmt) where global state is a major impediment to the sustainability of the code.

![Call graph of the initial state of the code, making heavy use of global state.]\(call-graphs/($globalised.short).svg)

The following commits are titled \"Shield {procedure} Step {i}\" and repeatedly apply the process outlined in my talk to the code until all global variables have been removed from the code. I will cover each of these commits here with the diff of the changes to `src` \(check out the commit to see the full changes), a visualisation of the call graph, and a short explanation of what is going on it that step. Be aware that commits are being used as a convenient way to present this process, in a real scenario you don't need to have a separate commit for each step \(though doing so may be useful).

The procedures in the call graphs are divided into three sections: \"Global-Heavy Code\", \"Adapter Boundary\", and \"Local-Only Code\". These are not actual sections in the code, they are conceptual sections in the call graph. \"Global-Heavy Code\" contains procedures that access global state, \"Local-Only Code\" contains procedures that explicitly pass state as procedure parameters, and \"Adapter Boundary\" contains procedures that are responsible for translating the old global-using interface to the new local-only interface. Depending on the facilities provided by your language it might make sense to have them be explicit in your code, for example if your language allows it then you could try to make sure that procedures in \"Local-Only Code\" can only be accessed from procedures in the \"Adapter Boundary\".

" | save --append $readme

let commits = (git log --format=format:'%h %H %s' | lines | find -r '^.+ Shield .+ Step .+$' | parse '{short} {long} Shield {object} Step {step}' | reverse)

for commit in $commits {
    git checkout $commit.long

    d2 --layout elk call-graph.d2 $"($graphs)/($commit.short).svg"

    $"# [($commit.short)]\(($gitlab)($commit.long)): Shield `($commit.object)` Step ($commit.step)\n\n" | save --append $readme
    $"![Call graph of step ($commit.step) of shielding ($commit.object).]\(call-graphs/($commit.short).svg)\n\n" | save --append $readme
    "<details>\n<summary>Diff Of `src/`</summary>\n\n~~~ diff\n" | save --append $readme
    git diff --no-ext-diff @^! src | save --append $readme
    "~~~\n\n</details>\n\n" | save --append $readme
    git show --no-patch --format=format:%b | str replace --all '([^\n])\n([^\n])' '$1 $2' | save --append $readme
    "\n\n" | save --append $readme
}

git checkout main

rm -rf call-graphs/
cp -r $graphs call-graphs
cp $readme README.md

git add call-graphs/ README.md
git commit --amend --no-edit