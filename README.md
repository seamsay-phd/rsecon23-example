# RSECon23 Legacy Refactoring Example

This is a git repository designed to act as an example of the process outlined in my RSECon23 talk "A Global Revolt". You can find a more thoroughly explained version of this process in the (currently in-progress) paper [found here](https://gitlab.com/seamsay-phd/refactor).

This repository uses the Fortran 90 Lithium example from Chapter 13.4 of Computational Quantum Mechanics by Izaac & Wang to provide a small but realistic example program. A one-to-one implementation (almost) of this code is available in [the first commit, 90481fa](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/90481fa9eca7ce6bc2f189931557aaceaa445402) (to their credit this version is very similar to the final version).

![Call graph of the code on which this example is based.](call-graphs/90481fa.svg)

The next commit ([a9daa2e](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/a9daa2ee6bc2887e881714c69630900760086d73)) contains a "globalised" version of that code, where a set of variables have been changed to global variables accessed by all parts of the code. While in a toy example like this readability and maintainability are not hugely affected, this work is based on [RMT](https://gitlab.com/Uk-amor/RMT/rmt) where global state is a major impediment to the sustainability of the code.

![Call graph of the initial state of the code, making heavy use of global state.](call-graphs/a9daa2e.svg)

The following commits are titled "Shield {procedure} Step {i}" and repeatedly apply the process outlined in my talk to the code until all global variables have been removed from the code. I will cover each of these commits here with the diff of the changes to `src` (check out the commit to see the full changes), a visualisation of the call graph, and a short explanation of what is going on it that step. Be aware that commits are being used as a convenient way to present this process, in a real scenario you don't need to have a separate commit for each step (though doing so may be useful).

The procedures in the call graphs are divided into three sections: "Global-Heavy Code", "Adapter Boundary", and "Local-Only Code". These are not actual sections in the code, they are conceptual sections in the call graph. "Global-Heavy Code" contains procedures that access global state, "Local-Only Code" contains procedures that explicitly pass state as procedure parameters, and "Adapter Boundary" contains procedures that are responsible for translating the old global-using interface to the new local-only interface. Depending on the facilities provided by your language it might make sense to have them be explicit in your code, for example if your language allows it then you could try to make sure that procedures in "Local-Only Code" can only be accessed from procedures in the "Adapter Boundary".

# [e85d2c6](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/e85d2c63a28f5072ef73e42102c11562629b8ced): Shield `numerov_cooley_single` Step 1

![Call graph of step 1 of shielding numerov_cooley_single.](call-graphs/e85d2c6.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/numerovcooley.f90 b/src/numerovcooley.f90
index 634087c..dcc570a 100644
--- a/src/numerovcooley.f90
+++ b/src/numerovcooley.f90
@@ -1,6 +1,8 @@
 module numerovcooley
     use iso_fortran_env, only: stderr => error_unit, int32, real64
     use cli, only: dr, N
+    use numerovcooley_s, only: numerov_cooley_single_s
+
     implicit none
 
     private
@@ -107,49 +109,6 @@ contains
         real(real64), intent(inout) :: E
         real(real64), intent(in) :: V(N)
 
-        real(real64) :: P(N), Y(N), k, dE
-        integer(int32) :: j, m
-
-        P = -2.0_real64 * (V - E)
-        k = dr**2 / 12.0_real64
-
-        psi = 0.0_real64
-        psi(2) = 1.0e-6_real64
-        psi(N - 1) = 1.0e-6_real64
-
-        m = N
-        do j = 2, N-1
-            psi(j + 1) = ( &
-                2.0_real64 * psi(j) * (1.0_real64 - 5.0_real64 * k * P(j)) - &
-                psi(j - 1) * (1.0_real64 + k * P(j - 1)) &
-            ) / (1.0_real64 + k * P(j - 1))
-
-            if (psi(j) < psi(j - 1)) then
-                m = j + 1
-                exit
-            end if
-        end do
-
-        psi(:m-1) = psi(:m-1) / psi(m)
-
-        do j = N-1, m+1, -1
-            psi(j - 1) = ( &
-                2.0_real64 * psi(j) * (1.0_real64 - 5.0_real64 * k * P(j)) - &
-                psi(j + 1) * (1.0_real64 + k * P(j + 1)) &
-            ) / (1.0_real64 + k * P(j + 1))
-        end do
-
-        psi(m:) = psi(m:) / psi(m)
-
-        ! Cooley correction.
-        Y = (1.0_real64 - 2.0_real64 * k * (V - E)) * psi
-        dE = &
-            (psi(m) / sum(abs(psi))**2) * &
-            ( &
-                -0.5_real64 * (Y(m + 1) - 2 * Y(m) + Y(m - 1)) / dr**2 + &
-                (V(m) - E) * psi(m) &
-            )
-
-        E = E + dE
+        call numerov_cooley_single_s(psi, E, V)
     end subroutine
-end module numerovcooley
\ No newline at end of file
+end module numerovcooley
diff --git a/src/numerovcooley_s.f90 b/src/numerovcooley_s.f90
new file mode 100644
index 0000000..e700590
--- /dev/null
+++ b/src/numerovcooley_s.f90
@@ -0,0 +1,60 @@
+module numerovcooley_s
+    use iso_fortran_env, only: int32, real64
+    use cli, only: dr, N
+
+    implicit none
+
+    private
+    public :: numerov_cooley_single_s
+contains
+    subroutine numerov_cooley_single_s(psi, E, V)
+        real(real64), intent(out) :: psi(N)
+        real(real64), intent(inout) :: E
+        real(real64), intent(in) :: V(N)
+
+        real(real64) :: P(N), Y(N), k, dE
+        integer(int32) :: j, m
+
+        P = -2.0_real64 * (V - E)
+        k = dr**2 / 12.0_real64
+
+        psi = 0.0_real64
+        psi(2) = 1.0e-6_real64
+        psi(N - 1) = 1.0e-6_real64
+
+        m = N
+        do j = 2, N-1
+            psi(j + 1) = ( &
+                2.0_real64 * psi(j) * (1.0_real64 - 5.0_real64 * k * P(j)) - &
+                psi(j - 1) * (1.0_real64 + k * P(j - 1)) &
+            ) / (1.0_real64 + k * P(j - 1))
+
+            if (psi(j) < psi(j - 1)) then
+                m = j + 1
+                exit
+            end if
+        end do
+
+        psi(:m-1) = psi(:m-1) / psi(m)
+
+        do j = N-1, m+1, -1
+            psi(j - 1) = ( &
+                2.0_real64 * psi(j) * (1.0_real64 - 5.0_real64 * k * P(j)) - &
+                psi(j + 1) * (1.0_real64 + k * P(j + 1)) &
+            ) / (1.0_real64 + k * P(j + 1))
+        end do
+
+        psi(m:) = psi(m:) / psi(m)
+
+        ! Cooley correction.
+        Y = (1.0_real64 - 2.0_real64 * k * (V - E)) * psi
+        dE = &
+            (psi(m) / sum(abs(psi))**2) * &
+            ( &
+                -0.5_real64 * (Y(m + 1) - 2 * Y(m) + Y(m - 1)) / dr**2 + &
+                (V(m) - E) * psi(m) &
+            )
+
+        E = E + dE
+    end subroutine
+end module numerovcooley_s
\ No newline at end of file
~~~

</details>

In this step the logic of the `numerov_cooley_single` procedure has been extracted into a new procedure `numerov_cooley_single_s`, the `_s` suffix in this case standing for "shielded" (because the original `numerov_cooley_single` procedure shields the new procedure's interface from the rest of the code). At this point the `numerov_cooley_single_s` procedure still accesses global state, and therefore can't be said to be part of the "Local-Only Code" section.

The new function has been put into its own module to aid the identification of global variables in a future step.

It is important to stress that _no logic changes_ should have occurred at this point, only the movement of code.


# [0ff93ad](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/0ff93adcb54e57aa1d6a0f11a2c3dc7754ca8ff0): Shield `numerov_cooley_single` Step 2

![Call graph of step 2 of shielding numerov_cooley_single.](call-graphs/0ff93ad.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/numerovcooley.f90 b/src/numerovcooley.f90
index dcc570a..d7af945 100644
--- a/src/numerovcooley.f90
+++ b/src/numerovcooley.f90
@@ -109,6 +109,6 @@ contains
         real(real64), intent(inout) :: E
         real(real64), intent(in) :: V(N)
 
-        call numerov_cooley_single_s(psi, E, V)
+        call numerov_cooley_single_s(psi, E, V, dr, N)
     end subroutine
 end module numerovcooley
diff --git a/src/numerovcooley_s.f90 b/src/numerovcooley_s.f90
index e700590..d9f658f 100644
--- a/src/numerovcooley_s.f90
+++ b/src/numerovcooley_s.f90
@@ -1,16 +1,16 @@
 module numerovcooley_s
     use iso_fortran_env, only: int32, real64
-    use cli, only: dr, N
 
     implicit none
 
     private
     public :: numerov_cooley_single_s
 contains
-    subroutine numerov_cooley_single_s(psi, E, V)
+    subroutine numerov_cooley_single_s(psi, E, V, dr, N)
+        integer(int32), intent(in) :: N
         real(real64), intent(out) :: psi(N)
         real(real64), intent(inout) :: E
-        real(real64), intent(in) :: V(N)
+        real(real64), intent(in) :: V(N), dr
 
         real(real64) :: P(N), Y(N), k, dE
         integer(int32) :: j, m
~~~

</details>

In this step the shielded `numerov_cooley_single_s` is changed to use local state only. Since the only procedure that uses it is `numerov_cooley_single` the changes to the code base are minimal. If the procedure had not been shielded, both `numerov_cooley` and `numerov_search` would have needed to be changed, increasing the risk of mistakes.

At this point the job of `numerov_cooley_single` is to translate the original global-using interface into the local-only interface provided `numerov_cooley_single_s`. It is therefore an adapter procedure. In the call graph pictured the set of procedures that act as adapters in this way are referred to as the "Adapter Boundary" and act to shield refactored code from the original code.


# [ac784cf](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/ac784cfd17271aa1310ca73fcdd4203e00860780): Shield `numerov_cooley_single` Step 3

![Call graph of step 3 of shielding numerov_cooley_single.](call-graphs/ac784cf.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/numerovcooley_s.f90 b/src/numerovcooley_s.f90
index d9f658f..ad85de2 100644
--- a/src/numerovcooley_s.f90
+++ b/src/numerovcooley_s.f90
@@ -22,8 +22,8 @@ contains
         psi(2) = 1.0e-6_real64
         psi(N - 1) = 1.0e-6_real64
 
-        m = N
-        do j = 2, N-1
+        m = N - 1
+        do j = 2, N-2
             psi(j + 1) = ( &
                 2.0_real64 * psi(j) * (1.0_real64 - 5.0_real64 * k * P(j)) - &
                 psi(j - 1) * (1.0_real64 + k * P(j - 1)) &
~~~

</details>

The move to local state only allowed unit tests to be written for `numerov_cooley_single_s`. Doing this uncovered a bug in the code when wavefunctions have no nodes.


# [0bfc06d](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/0bfc06d974959ea15b001b981e38b498ae37cd7b): Shield `numerov_search` Step 1

![Call graph of step 1 of shielding numerov_search.](call-graphs/0bfc06d.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/CMakeLists.txt b/src/CMakeLists.txt
index cda8feb..2103b57 100644
--- a/src/CMakeLists.txt
+++ b/src/CMakeLists.txt
@@ -4,6 +4,7 @@ set(
     hartree.f90
     integration.f90
     numerovcooley.f90
+    numerovcooley_s.f90
     CACHE INTERNAL ""
 )
 
diff --git a/src/numerovcooley.f90 b/src/numerovcooley.f90
index d7af945..3966619 100644
--- a/src/numerovcooley.f90
+++ b/src/numerovcooley.f90
@@ -16,6 +16,17 @@ contains
         integer(int32), intent(in), optional :: max_iters
 
         integer(int32), parameter :: cooley_convergence_iters = 10000
+
+        call numerov_search_s(psi, E, V, lower, upper, target_nodes, max_iters)
+        call numerov_cooley(psi, E, V, max_iters = cooley_convergence_iters)
+    end subroutine
+
+    subroutine numerov_search_s(psi, E, V, lower, upper, target_nodes, max_iters)
+        integer(int32), intent(in) :: target_nodes
+        real(real64), intent(out) :: psi(N), E
+        real(real64), intent(in) :: V(N), lower, upper
+        integer(int32), intent(in), optional :: max_iters
+
         integer(int32) :: nodes, i, j, max_iters_inner
         real(real64) :: prev_E, prev_psi, lower_inner, upper_inner
 
@@ -65,8 +76,6 @@ contains
                 error stop "Numerov node counting bisection failed."
             end if
         end do
-
-        call numerov_cooley(psi, E, V, max_iters = cooley_convergence_iters)
     end subroutine
 
     subroutine numerov_cooley(psi, E, V, max_iters)
~~~

</details>

In this step the shielding process has begun on the `numerov_search` procedure. In a more realistic scenario, you probably would have chosen `numerov_cooley` to refactor next as that procedure does not call any functions that aren't already in the adapter boundary. However, this procedure was chosen to demonstrate how a function could be partially shielded if that is something that is required.

Since this procedure calls `numerov_cooley` which is not yet behind the adapter boundary it can't be fully shielded. Instead only the part of the procedure that interacts with global state directly but does not itself call procedures that interact with global state is extracted. If the call to `numerov_cooley` had also been extracted then `numerov_search_s` would not be able to be moved into the "Local-Only Code" section in the next step as it would call a procedure that uses global state.

The procedure is not moved to the `numerovcooley_s` module to avoid dealing with circular dependencies. Instead it will be moved in the next step.


# [4a52054](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/4a520541b7be3f066c8adcea5f8a9e0b2a147345): Shield `numerov_search` Step 2

![Call graph of step 2 of shielding numerov_search.](call-graphs/4a52054.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/numerovcooley.f90 b/src/numerovcooley.f90
index 3966619..9bd843c 100644
--- a/src/numerovcooley.f90
+++ b/src/numerovcooley.f90
@@ -1,7 +1,7 @@
 module numerovcooley
     use iso_fortran_env, only: stderr => error_unit, int32, real64
     use cli, only: dr, N
-    use numerovcooley_s, only: numerov_cooley_single_s
+    use numerovcooley_s, only: numerov_cooley_single_s, numerov_search_s
 
     implicit none
 
@@ -21,63 +21,6 @@ contains
         call numerov_cooley(psi, E, V, max_iters = cooley_convergence_iters)
     end subroutine
 
-    subroutine numerov_search_s(psi, E, V, lower, upper, target_nodes, max_iters)
-        integer(int32), intent(in) :: target_nodes
-        real(real64), intent(out) :: psi(N), E
-        real(real64), intent(in) :: V(N), lower, upper
-        integer(int32), intent(in), optional :: max_iters
-
-        integer(int32) :: nodes, i, j, max_iters_inner
-        real(real64) :: prev_E, prev_psi, lower_inner, upper_inner
-
-        ! Changing `intent(in)` parameters is not allowed, so this is required instead...
-        if (present(max_iters)) then
-            max_iters_inner = max_iters
-        else
-            max_iters_inner = 10
-        end if
-        lower_inner = lower
-        upper_inner = upper
-
-        do i = 1, max_iters_inner
-            E = lower_inner + (upper_inner - lower_inner) / 2.0_real64
-            prev_E = E
-            call numerov_cooley_single(psi, E, V)
-
-            nodes = 0
-            prev_psi = psi(1)
-            do j = 2, N
-                ! NOTE: Assume none of the points are ever zero.
-                if (psi(j) * prev_psi < 0.0_real64) then
-                    nodes = nodes + 1
-                end if
-                prev_psi = psi(j)
-            end do
-
-            if (mod(i - 1, max_iters_inner / 10) == 0) then
-                write (stderr, *) &
-                    "Bisection Iteration", i, &
-                    "lower", lower_inner, &
-                    "upper", upper_inner, &
-                    "E", prev_E, &
-                    "E + dE", E, &
-                    "nodes", nodes
-            end if
-
-            if (nodes > target_nodes) then
-                upper_inner = prev_E
-            else if (nodes < target_nodes) then
-                lower_inner = prev_E
-            else
-                exit
-            end if
-
-            if (i == max_iters_inner) then
-                error stop "Numerov node counting bisection failed."
-            end if
-        end do
-    end subroutine
-
     subroutine numerov_cooley(psi, E, V, max_iters)
         real(real64), intent(out) :: psi(N)
         real(real64), intent(inout) :: E
diff --git a/src/numerovcooley_s.f90 b/src/numerovcooley_s.f90
index ad85de2..bfbed44 100644
--- a/src/numerovcooley_s.f90
+++ b/src/numerovcooley_s.f90
@@ -1,11 +1,69 @@
 module numerovcooley_s
-    use iso_fortran_env, only: int32, real64
+    use iso_fortran_env, only: stderr => error_unit, int32, real64
+    use cli, only: dr, N
 
     implicit none
 
     private
-    public :: numerov_cooley_single_s
+    public :: numerov_cooley_single_s, numerov_search_s
 contains
+    subroutine numerov_search_s(psi, E, V, lower, upper, target_nodes, max_iters)
+        integer(int32), intent(in) :: target_nodes
+        real(real64), intent(out) :: psi(N), E
+        real(real64), intent(in) :: V(N), lower, upper
+        integer(int32), intent(in), optional :: max_iters
+
+        integer(int32) :: nodes, i, j, max_iters_inner
+        real(real64) :: prev_E, prev_psi, lower_inner, upper_inner
+
+        ! Changing `intent(in)` parameters is not allowed, so this is required instead...
+        if (present(max_iters)) then
+            max_iters_inner = max_iters
+        else
+            max_iters_inner = 10
+        end if
+        lower_inner = lower
+        upper_inner = upper
+
+        do i = 1, max_iters_inner
+            E = lower_inner + (upper_inner - lower_inner) / 2.0_real64
+            prev_E = E
+            call numerov_cooley_single_s(psi, E, V, dr, N)
+
+            nodes = 0
+            prev_psi = psi(1)
+            do j = 2, N
+                ! NOTE: Assume none of the points are ever zero.
+                if (psi(j) * prev_psi < 0.0_real64) then
+                    nodes = nodes + 1
+                end if
+                prev_psi = psi(j)
+            end do
+
+            if (mod(i - 1, max_iters_inner / 10) == 0) then
+                write (stderr, *) &
+                    "Bisection Iteration", i, &
+                    "lower", lower_inner, &
+                    "upper", upper_inner, &
+                    "E", prev_E, &
+                    "E + dE", E, &
+                    "nodes", nodes
+            end if
+
+            if (nodes > target_nodes) then
+                upper_inner = prev_E
+            else if (nodes < target_nodes) then
+                lower_inner = prev_E
+            else
+                exit
+            end if
+
+            if (i == max_iters_inner) then
+                error stop "Numerov node counting bisection failed."
+            end if
+        end do
+    end subroutine
+
     subroutine numerov_cooley_single_s(psi, E, V, dr, N)
         integer(int32), intent(in) :: N
         real(real64), intent(out) :: psi(N)
~~~

</details>

In this step the shielded `numerov_search_s` procedure has been changed to remove any references to procedures that are in the adapter boundary, instead calling their shielded equivalents. This allows it to be moved into the `numerovcooley_s` module without causing circular dependencies which makes it easier to find global variables in the code.


# [674ab7c](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/674ab7c4818dde159c61e4c4584f9dd69bc5371f): Shield `numerov_search` Step 3

![Call graph of step 3 of shielding numerov_search.](call-graphs/674ab7c.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/numerovcooley.f90 b/src/numerovcooley.f90
index 9bd843c..d9153b0 100644
--- a/src/numerovcooley.f90
+++ b/src/numerovcooley.f90
@@ -17,7 +17,7 @@ contains
 
         integer(int32), parameter :: cooley_convergence_iters = 10000
 
-        call numerov_search_s(psi, E, V, lower, upper, target_nodes, max_iters)
+        call numerov_search_s(psi, E, V, lower, upper, dr, N, target_nodes, max_iters)
         call numerov_cooley(psi, E, V, max_iters = cooley_convergence_iters)
     end subroutine
 
diff --git a/src/numerovcooley_s.f90 b/src/numerovcooley_s.f90
index bfbed44..001af29 100644
--- a/src/numerovcooley_s.f90
+++ b/src/numerovcooley_s.f90
@@ -1,16 +1,15 @@
 module numerovcooley_s
     use iso_fortran_env, only: stderr => error_unit, int32, real64
-    use cli, only: dr, N
 
     implicit none
 
     private
     public :: numerov_cooley_single_s, numerov_search_s
 contains
-    subroutine numerov_search_s(psi, E, V, lower, upper, target_nodes, max_iters)
-        integer(int32), intent(in) :: target_nodes
+    subroutine numerov_search_s(psi, E, V, lower, upper, dr, N, target_nodes, max_iters)
+        integer(int32), intent(in) :: N, target_nodes
         real(real64), intent(out) :: psi(N), E
-        real(real64), intent(in) :: V(N), lower, upper
+        real(real64), intent(in) :: dr, V(N), lower, upper
         integer(int32), intent(in), optional :: max_iters
 
         integer(int32) :: nodes, i, j, max_iters_inner
~~~

</details>

In this step `numerov_search_s` has been changed to operate only on local state, setting it up to be unit tested effectively. Note that `numerov_search` is an adapter procedure that calls a procedure in the "Global-Heavy Code" section.


# [a81be79](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/a81be79db2da03fb8a83771690cc8d6aeba530f1): Shield `numerov_search` Step 4

![Call graph of step 4 of shielding numerov_search.](call-graphs/a81be79.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
~~~

</details>

Now that `numerov_search_s` operates only on local state, it can be unit tested.

Note that these particular unit tests could have been written even when `numerov_search` was operating on global state, since they are not varying `dr` or `N`. However these are just examples and comprehensive tests would need to operate on a variety of grid sizes and spacings.


# [98622c3](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/98622c3219762279e2a48895ef3d06cf6ea7def7): Shield `numerov_cooley` Step 1

![Call graph of step 1 of shielding numerov_cooley.](call-graphs/98622c3.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/numerovcooley.f90 b/src/numerovcooley.f90
index d9153b0..1ef00cc 100644
--- a/src/numerovcooley.f90
+++ b/src/numerovcooley.f90
@@ -27,6 +27,15 @@ contains
         real(real64), intent(in) :: V(N)
         integer(int32), intent(in), optional :: max_iters
 
+        call numerov_cooley_s(psi, E, V, max_iters)
+    end subroutine
+
+    subroutine numerov_cooley_s(psi, E, V, max_iters)
+        real(real64), intent(out) :: psi(N)
+        real(real64), intent(inout) :: E
+        real(real64), intent(in) :: V(N)
+        integer(int32), intent(in), optional :: max_iters
+
         integer(int32) :: i, max_iters_inner
         real(real64) :: prev_E, dE
 
~~~

</details>

In this step the `numerov_cooley` procedure has been shielded. There is nothing in this commit that hasn't been shown before.


# [aec793e](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/aec793e76edb64542c00810567ed21c507daafaf): Shield `numerov_cooley` Step 2

![Call graph of step 2 of shielding numerov_cooley.](call-graphs/aec793e.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/numerovcooley.f90 b/src/numerovcooley.f90
index 1ef00cc..27787cb 100644
--- a/src/numerovcooley.f90
+++ b/src/numerovcooley.f90
@@ -1,7 +1,7 @@
 module numerovcooley
     use iso_fortran_env, only: stderr => error_unit, int32, real64
     use cli, only: dr, N
-    use numerovcooley_s, only: numerov_cooley_single_s, numerov_search_s
+    use numerovcooley_s, only: numerov_cooley_s, numerov_cooley_single_s, numerov_search_s
 
     implicit none
 
@@ -29,47 +29,4 @@ contains
 
         call numerov_cooley_s(psi, E, V, max_iters)
     end subroutine
-
-    subroutine numerov_cooley_s(psi, E, V, max_iters)
-        real(real64), intent(out) :: psi(N)
-        real(real64), intent(inout) :: E
-        real(real64), intent(in) :: V(N)
-        integer(int32), intent(in), optional :: max_iters
-
-        integer(int32) :: i, max_iters_inner
-        real(real64) :: prev_E, dE
-
-        ! Changing `intent(in)` parameters is not allowed, so this is required instead...
-        if (present(max_iters)) then
-            max_iters_inner = max_iters
-        else
-            max_iters_inner = 10000
-        end if
-
-        do i = 1, max_iters_inner
-            prev_E = E
-            call numerov_cooley_single(psi, E, V)
-            dE = abs(prev_E - E)
-
-            if (mod(i - 1, max_iters_inner / 10) == 0) then
-                write (stderr, *) &
-                    "Numerov-Cooley Iteration", i, &
-                    "E", prev_E, &
-                    "dE", dE, &
-                    "E + dE", E
-            end if
-
-            if (dE < 1.0e-6_real64) then
-                exit
-            end if
-        end do
-    end subroutine
-
-    subroutine numerov_cooley_single(psi, E, V)
-        real(real64), intent(out) :: psi(N)
-        real(real64), intent(inout) :: E
-        real(real64), intent(in) :: V(N)
-
-        call numerov_cooley_single_s(psi, E, V, dr, N)
-    end subroutine
 end module numerovcooley
diff --git a/src/numerovcooley_s.f90 b/src/numerovcooley_s.f90
index 001af29..15ce9ac 100644
--- a/src/numerovcooley_s.f90
+++ b/src/numerovcooley_s.f90
@@ -1,10 +1,11 @@
 module numerovcooley_s
     use iso_fortran_env, only: stderr => error_unit, int32, real64
+    use cli, only: dr, N
 
     implicit none
 
     private
-    public :: numerov_cooley_single_s, numerov_search_s
+    public :: numerov_cooley_s, numerov_cooley_single_s, numerov_search_s
 contains
     subroutine numerov_search_s(psi, E, V, lower, upper, dr, N, target_nodes, max_iters)
         integer(int32), intent(in) :: N, target_nodes
@@ -63,6 +64,41 @@ contains
         end do
     end subroutine
 
+    subroutine numerov_cooley_s(psi, E, V, max_iters)
+        real(real64), intent(out) :: psi(N)
+        real(real64), intent(inout) :: E
+        real(real64), intent(in) :: V(N)
+        integer(int32), intent(in), optional :: max_iters
+
+        integer(int32) :: i, max_iters_inner
+        real(real64) :: prev_E, dE
+
+        ! Changing `intent(in)` parameters is not allowed, so this is required instead...
+        if (present(max_iters)) then
+            max_iters_inner = max_iters
+        else
+            max_iters_inner = 10000
+        end if
+
+        do i = 1, max_iters_inner
+            prev_E = E
+            call numerov_cooley_single_s(psi, E, V, dr, N)
+            dE = abs(prev_E - E)
+
+            if (mod(i - 1, max_iters_inner / 10) == 0) then
+                write (stderr, *) &
+                    "Numerov-Cooley Iteration", i, &
+                    "E", prev_E, &
+                    "dE", dE, &
+                    "E + dE", E
+            end if
+
+            if (dE < 1.0e-6_real64) then
+                exit
+            end if
+        end do
+    end subroutine
+
     subroutine numerov_cooley_single_s(psi, E, V, dr, N)
         integer(int32), intent(in) :: N
         real(real64), intent(out) :: psi(N)
~~~

</details>

In this step the shielded `numerov_cooley_s` has been changed to call shielded procedures instead of procedures in the adapter boundary. This step was performed so that `numerov_cooley_s` could be moved into the `numerovcooley_s` module, making the identification of global variables easier. Now that `numerov_cooley_s` no longer calls `numerov_cooley_single` it can safely be deleted.


# [1e881e0](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/1e881e0e83c29216cdd4463419c58b2c4f6a13d6): Shield `numerov_cooley` Step 3

![Call graph of step 3 of shielding numerov_cooley.](call-graphs/1e881e0.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/numerovcooley.f90 b/src/numerovcooley.f90
index 27787cb..7e809f7 100644
--- a/src/numerovcooley.f90
+++ b/src/numerovcooley.f90
@@ -27,6 +27,6 @@ contains
         real(real64), intent(in) :: V(N)
         integer(int32), intent(in), optional :: max_iters
 
-        call numerov_cooley_s(psi, E, V, max_iters)
+        call numerov_cooley_s(psi, E, V, dr, N, max_iters)
     end subroutine
 end module numerovcooley
diff --git a/src/numerovcooley_s.f90 b/src/numerovcooley_s.f90
index 15ce9ac..6274929 100644
--- a/src/numerovcooley_s.f90
+++ b/src/numerovcooley_s.f90
@@ -1,6 +1,5 @@
 module numerovcooley_s
     use iso_fortran_env, only: stderr => error_unit, int32, real64
-    use cli, only: dr, N
 
     implicit none
 
@@ -64,10 +63,11 @@ contains
         end do
     end subroutine
 
-    subroutine numerov_cooley_s(psi, E, V, max_iters)
+    subroutine numerov_cooley_s(psi, E, V, dr, N, max_iters)
+        integer(int32), intent(in) :: N
         real(real64), intent(out) :: psi(N)
         real(real64), intent(inout) :: E
-        real(real64), intent(in) :: V(N)
+        real(real64), intent(in) :: dr, V(N)
         integer(int32), intent(in), optional :: max_iters
 
         integer(int32) :: i, max_iters_inner
~~~

</details>

In this step `numerov_cooley_s` has been changed to operate on local state only, allowing it to be unit tested thoroughly.

Note that `numerov_search` is no longer calling a global-heavy procedure but `numerov_search_s` has not yet been changed to call `numerov_cooley_s` directly yet.


# [bf906b5](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/bf906b5dfe342cd2c359643992123727f100b13e): Shield `numerov_cooley` Step 4

![Call graph of step 4 of shielding numerov_cooley.](call-graphs/bf906b5.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
~~~

</details>

Unit tests have been written for `numerov_cooley_s` (for some arbitrary definition of "written").


# [5bc1433](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/5bc14336b36e750e3a12ae00e2ff2793cd476506): Shield `numerov_cooley` Step 5

![Call graph of step 5 of shielding numerov_cooley.](call-graphs/5bc1433.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/numerovcooley.f90 b/src/numerovcooley.f90
index 7e809f7..4252d89 100644
--- a/src/numerovcooley.f90
+++ b/src/numerovcooley.f90
@@ -1,7 +1,7 @@
 module numerovcooley
     use iso_fortran_env, only: stderr => error_unit, int32, real64
     use cli, only: dr, N
-    use numerovcooley_s, only: numerov_cooley_s, numerov_cooley_single_s, numerov_search_s
+    use numerovcooley_s, only: numerov_cooley_s, numerov_cooley_single_s, numerov_search_bisection_s
 
     implicit none
 
@@ -17,7 +17,7 @@ contains
 
         integer(int32), parameter :: cooley_convergence_iters = 10000
 
-        call numerov_search_s(psi, E, V, lower, upper, dr, N, target_nodes, max_iters)
+        call numerov_search_bisection_s(psi, E, V, lower, upper, dr, N, target_nodes, max_iters)
         call numerov_cooley(psi, E, V, max_iters = cooley_convergence_iters)
     end subroutine
 
diff --git a/src/numerovcooley_s.f90 b/src/numerovcooley_s.f90
index 6274929..13dc525 100644
--- a/src/numerovcooley_s.f90
+++ b/src/numerovcooley_s.f90
@@ -4,9 +4,9 @@ module numerovcooley_s
     implicit none
 
     private
-    public :: numerov_cooley_s, numerov_cooley_single_s, numerov_search_s
+    public :: numerov_cooley_s, numerov_cooley_single_s, numerov_search_bisection_s
 contains
-    subroutine numerov_search_s(psi, E, V, lower, upper, dr, N, target_nodes, max_iters)
+    subroutine numerov_search_bisection_s(psi, E, V, lower, upper, dr, N, target_nodes, max_iters)
         integer(int32), intent(in) :: N, target_nodes
         real(real64), intent(out) :: psi(N), E
         real(real64), intent(in) :: dr, V(N), lower, upper
~~~

</details>

Now that `numerov_cooley` has a unit-testable equivalent, `numerov_search_s` can be changed to use that instead of `numerov_search` calling `numerov_search_s` and `numerov_cooley`. However, to ensure that we don't make any mistakes during that process we need to make sure that the current `numerov_search_s` procedure is still unit tested while that change is taking place. `numerov_search_s` has been renamed so that it can be unit tested alongside the new `numerov_search_s` procedure.


# [19969b9](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/19969b9a05c43a016da570638b01888a2a3df268): Shield `numerov_cooley` Step 6

![Call graph of step 6 of shielding numerov_cooley.](call-graphs/19969b9.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/numerovcooley.f90 b/src/numerovcooley.f90
index 4252d89..7373dbf 100644
--- a/src/numerovcooley.f90
+++ b/src/numerovcooley.f90
@@ -1,7 +1,7 @@
 module numerovcooley
     use iso_fortran_env, only: stderr => error_unit, int32, real64
     use cli, only: dr, N
-    use numerovcooley_s, only: numerov_cooley_s, numerov_cooley_single_s, numerov_search_bisection_s
+    use numerovcooley_s, only: numerov_cooley_s, numerov_cooley_single_s, numerov_search_s
 
     implicit none
 
@@ -17,8 +17,7 @@ contains
 
         integer(int32), parameter :: cooley_convergence_iters = 10000
 
-        call numerov_search_bisection_s(psi, E, V, lower, upper, dr, N, target_nodes, max_iters)
-        call numerov_cooley(psi, E, V, max_iters = cooley_convergence_iters)
+        call numerov_search_s(psi, E, V, dr, N, lower, upper, target_nodes, max_iters, cooley_convergence_iters)
     end subroutine
 
     subroutine numerov_cooley(psi, E, V, max_iters)
diff --git a/src/numerovcooley_s.f90 b/src/numerovcooley_s.f90
index 13dc525..841120d 100644
--- a/src/numerovcooley_s.f90
+++ b/src/numerovcooley_s.f90
@@ -4,8 +4,18 @@ module numerovcooley_s
     implicit none
 
     private
-    public :: numerov_cooley_s, numerov_cooley_single_s, numerov_search_bisection_s
+    public :: numerov_cooley_s, numerov_cooley_single_s, numerov_search_bisection_s, numerov_search_s
 contains
+    subroutine numerov_search_s(psi, E, V, dr, N, lower, upper, target_nodes, max_iters, cooley_convergence_iters)
+        integer(int32), intent(in) :: N, target_nodes
+        real(real64), intent(out) :: psi(N), E
+        real(real64), intent(in) :: dr, V(N), lower, upper
+        integer(int32), intent(in), optional :: max_iters, cooley_convergence_iters
+
+        call numerov_search_bisection_s(psi, E, V, lower, upper, dr, N, target_nodes, max_iters)
+        call numerov_cooley_s(psi, E, V, dr, N, cooley_convergence_iters)
+    end subroutine
+
     subroutine numerov_search_bisection_s(psi, E, V, lower, upper, dr, N, target_nodes, max_iters)
         integer(int32), intent(in) :: N, target_nodes
         real(real64), intent(out) :: psi(N), E
~~~

</details>

A new `numerov_search_s` has been introduced that calls both `numerov_search_bisection_s` and then `numerov_cooley_s`, making it a true shielded equivalent of `numerov_search`. It has also been unit tested without removing the unit tests of `numerov_search_bisection_s`.


# [71517ee](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/71517eea3fe45ee868f21c0b4f276662b332cc0d): Shield `numerov_cooley` Step 7

![Call graph of step 7 of shielding numerov_cooley.](call-graphs/71517ee.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/numerovcooley_s.f90 b/src/numerovcooley_s.f90
index 841120d..b100d09 100644
--- a/src/numerovcooley_s.f90
+++ b/src/numerovcooley_s.f90
@@ -4,7 +4,7 @@ module numerovcooley_s
     implicit none
 
     private
-    public :: numerov_cooley_s, numerov_cooley_single_s, numerov_search_bisection_s, numerov_search_s
+    public :: numerov_cooley_s, numerov_cooley_single_s, numerov_search_s
 contains
     subroutine numerov_search_s(psi, E, V, dr, N, lower, upper, target_nodes, max_iters, cooley_convergence_iters)
         integer(int32), intent(in) :: N, target_nodes
@@ -12,18 +12,8 @@ contains
         real(real64), intent(in) :: dr, V(N), lower, upper
         integer(int32), intent(in), optional :: max_iters, cooley_convergence_iters
 
-        call numerov_search_bisection_s(psi, E, V, lower, upper, dr, N, target_nodes, max_iters)
-        call numerov_cooley_s(psi, E, V, dr, N, cooley_convergence_iters)
-    end subroutine
-
-    subroutine numerov_search_bisection_s(psi, E, V, lower, upper, dr, N, target_nodes, max_iters)
-        integer(int32), intent(in) :: N, target_nodes
-        real(real64), intent(out) :: psi(N), E
-        real(real64), intent(in) :: dr, V(N), lower, upper
-        integer(int32), intent(in), optional :: max_iters
-
-        integer(int32) :: nodes, i, j, max_iters_inner
-        real(real64) :: prev_E, prev_psi, lower_inner, upper_inner
+        integer(int32) :: max_iters_inner, nodes, i, j
+        real(real64) :: lower_inner, upper_inner, prev_E, prev_psi
 
         ! Changing `intent(in)` parameters is not allowed, so this is required instead...
         if (present(max_iters)) then
@@ -71,6 +61,8 @@ contains
                 error stop "Numerov node counting bisection failed."
             end if
         end do
+
+        call numerov_cooley_s(psi, E, V, dr, N, cooley_convergence_iters)
     end subroutine
 
     subroutine numerov_cooley_s(psi, E, V, dr, N, max_iters)
~~~

</details>

Since the unit tests for `numerov_search_s` and `numerov_search_bisection_s` were both passing in the last step we have moved the `numerov_search_bisection_s` code back into `numerov_search_s` and are confident that this has not broken the logic since `numerov_search_s` unit tests were written against a known working version.


# [91a6afe](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/91a6afe480f8695dde57190594d1cb684c4a889d): Shield `simpsons` Step 1

![Call graph of step 1 of shielding simpsons.](call-graphs/91a6afe.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/CMakeLists.txt b/src/CMakeLists.txt
index 2103b57..7f77a85 100644
--- a/src/CMakeLists.txt
+++ b/src/CMakeLists.txt
@@ -3,6 +3,7 @@ set(
     cli.f90
     hartree.f90
     integration.f90
+    integration_s.f90
     numerovcooley.f90
     numerovcooley_s.f90
     CACHE INTERNAL ""
diff --git a/src/integration.f90 b/src/integration.f90
index 8f5eec0..0704bb9 100644
--- a/src/integration.f90
+++ b/src/integration.f90
@@ -1,6 +1,7 @@
 module integration
     use iso_fortran_env, only: int32, real64
     use cli, only: dr, N
+    use integration_s, only: simpsons_s
 
     implicit none
 
@@ -21,23 +22,6 @@ contains
         real(real64), intent(in) :: array(N)
         real(real64) :: simpsons
 
-        logical :: is_odd
-        integer(int32) :: N_inner
-
-        is_odd = mod(N, 2) /= 0
-        if (is_odd) then
-            N_inner = N - 1
-        else
-            N_inner = N
-        end if
-
-        simpsons = array(1) + 4*sum(array(2:N_inner-1:2)) + 2*sum(array(3:N_inner-2:2)) + array(N_inner)
-        simpsons = simpsons * dr / 3.0_real64
-
-        if (is_odd) then
-            ! As a hack, add a single trapezoid rule evaluation onto the end if the array length is odd.
-            simpsons = simpsons + (array(N - 1) + array(N)) * dr / 2.0_real64
-        end if
-
+        simpsons = simpsons_s(array, N)
     end function
 end module integration
\ No newline at end of file
diff --git a/src/integration_s.f90 b/src/integration_s.f90
new file mode 100644
index 0000000..e35dd75
--- /dev/null
+++ b/src/integration_s.f90
@@ -0,0 +1,32 @@
+module integration_s
+    use iso_fortran_env, only: int32, real64
+    use cli, only: dr
+
+    implicit none
+
+    public :: simpsons_s
+contains
+    function simpsons_s(array, N) result(simpsons)
+        integer(int32), intent(in) :: N
+        real(real64), intent(in) :: array(N)
+        real(real64) :: simpsons
+
+        logical :: is_odd
+        integer(int32) :: N_inner
+
+        is_odd = mod(N, 2) /= 0
+        if (is_odd) then
+            N_inner = N - 1
+        else
+            N_inner = N
+        end if
+
+        simpsons = array(1) + 4*sum(array(2:N_inner-1:2)) + 2*sum(array(3:N_inner-2:2)) + array(N_inner)
+        simpsons = simpsons * dr / 3.0_real64
+
+        if (is_odd) then
+            ! As a hack, add a single trapezoid rule evaluation onto the end if the array length is odd.
+            simpsons = simpsons + (array(N - 1) + array(N)) * dr / 2.0_real64
+        end if
+    end function
+end module integration_s
\ No newline at end of file
~~~

</details>



# [d53a582](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/d53a58234c26a27bd077292b602aa981eb1ee59c): Shield `simpsons` Step 2

![Call graph of step 2 of shielding simpsons.](call-graphs/d53a582.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/integration.f90 b/src/integration.f90
index 0704bb9..ca833ac 100644
--- a/src/integration.f90
+++ b/src/integration.f90
@@ -22,6 +22,6 @@ contains
         real(real64), intent(in) :: array(N)
         real(real64) :: simpsons
 
-        simpsons = simpsons_s(array, N)
+        simpsons = simpsons_s(array, dr, N)
     end function
 end module integration
\ No newline at end of file
diff --git a/src/integration_s.f90 b/src/integration_s.f90
index e35dd75..5ca71a3 100644
--- a/src/integration_s.f90
+++ b/src/integration_s.f90
@@ -1,14 +1,13 @@
 module integration_s
     use iso_fortran_env, only: int32, real64
-    use cli, only: dr
 
     implicit none
 
     public :: simpsons_s
 contains
-    function simpsons_s(array, N) result(simpsons)
+    function simpsons_s(array, dr, N) result(simpsons)
         integer(int32), intent(in) :: N
-        real(real64), intent(in) :: array(N)
+        real(real64), intent(in) :: dr, array(N)
         real(real64) :: simpsons
 
         logical :: is_odd
~~~

</details>



# [a0c69d3](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/a0c69d3c4737f5c0ef1ba83930e19bb5f16cf6c0): Shield `simpsons` Step 3

![Call graph of step 3 of shielding simpsons.](call-graphs/a0c69d3.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
~~~

</details>



# [adee2ca](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/adee2ca99b5f27fbfede89e6b201e1eae4ed9207): Shield `normalise` Step 1

![Call graph of step 1 of shielding normalise.](call-graphs/adee2ca.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/integration.f90 b/src/integration.f90
index ca833ac..000c9b3 100644
--- a/src/integration.f90
+++ b/src/integration.f90
@@ -11,6 +11,12 @@ contains
     subroutine normalise(psi)
         real(real64), intent(inout) :: psi(N)
 
+        call normalise_s(psi)
+    end subroutine
+
+    subroutine normalise_s(psi)
+        real(real64), intent(inout) :: psi(N)
+
         real(real64) :: area
 
         area = simpsons(psi**2, N)
~~~

</details>



# [4367fdd](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/4367fdd0d759f953dc0958eca397ba1ab7355ece): Shield `normalise` Step 2

![Call graph of step 2 of shielding normalise.](call-graphs/4367fdd.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/integration.f90 b/src/integration.f90
index 000c9b3..a1f3d19 100644
--- a/src/integration.f90
+++ b/src/integration.f90
@@ -1,7 +1,7 @@
 module integration
     use iso_fortran_env, only: int32, real64
     use cli, only: dr, N
-    use integration_s, only: simpsons_s
+    use integration_s, only: normalise_s, simpsons_s
 
     implicit none
 
@@ -11,16 +11,7 @@ contains
     subroutine normalise(psi)
         real(real64), intent(inout) :: psi(N)
 
-        call normalise_s(psi)
-    end subroutine
-
-    subroutine normalise_s(psi)
-        real(real64), intent(inout) :: psi(N)
-
-        real(real64) :: area
-
-        area = simpsons(psi**2, N)
-        psi = psi / sqrt(area)
+        call normalise_s(psi, dr, N)
     end subroutine
 
     function simpsons(array, N)
diff --git a/src/integration_s.f90 b/src/integration_s.f90
index 5ca71a3..6f18ad4 100644
--- a/src/integration_s.f90
+++ b/src/integration_s.f90
@@ -3,8 +3,19 @@ module integration_s
 
     implicit none
 
-    public :: simpsons_s
+    public :: normalise_s, simpsons_s
 contains
+    subroutine normalise_s(psi, dr, N)
+        integer(int32), intent(in) :: N
+        real(real64), intent(inout) :: psi(N)
+        real(real64), intent(in) :: dr
+
+        real(real64) :: area
+
+        area = simpsons_s(psi**2, dr, N)
+        psi = psi / sqrt(area)
+    end subroutine
+
     function simpsons_s(array, dr, N) result(simpsons)
         integer(int32), intent(in) :: N
         real(real64), intent(in) :: dr, array(N)
~~~

</details>



# [584213b](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/584213bf07b0b876d12a0de1ce4bac284daf8b84): Shield `normalise` Step 3

![Call graph of step 3 of shielding normalise.](call-graphs/584213b.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
~~~

</details>



# [8dde12e](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/8dde12eaae7c5080dc5068f76a5bb0eb72408266): Shield `hartree_method` Step 1

![Call graph of step 1 of shielding hartree_method.](call-graphs/8dde12e.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/CMakeLists.txt b/src/CMakeLists.txt
index 7f77a85..8ff14de 100644
--- a/src/CMakeLists.txt
+++ b/src/CMakeLists.txt
@@ -2,6 +2,7 @@ set(
     MODULES
     cli.f90
     hartree.f90
+    hartree_s.f90
     integration.f90
     integration_s.f90
     numerovcooley.f90
diff --git a/src/hartree.f90 b/src/hartree.f90
index 8031f0b..4ae8291 100644
--- a/src/hartree.f90
+++ b/src/hartree.f90
@@ -5,8 +5,7 @@ module hartree
         dr, E, E_adj, &
         psi, &
         N, Z
-    use integration, only: normalise, simpsons
-    use numerovcooley, only: numerov_search, numerov_cooley
+    use hartree_s, only: hartree_method_s
 
     implicit none
 
@@ -20,46 +19,6 @@ contains
     end subroutine
 
     subroutine hartree_method
-        integer(int32), parameter :: hartree_iterations = 5
-        real(real64) :: r(N), r_min, V(N), Qenc(N), rho(N)
-        integer(int32) :: i, j, k
-
-        r_min = 1.0e-5_real64
-        r = [(r_min + dr*i, i=0,N-1)]
-        psi = 0.0_real64
-        V = -Z/r
-
-        call numerov_search(psi(:, 1), E(1), V, -10.0_real64, -1.0_real64, 0)
-        call numerov_search(psi(:, 2), E(2), V, -10.0_real64, -1.0_real64, 0)
-        call numerov_search(psi(:, 3), E(3), V, -10.0_real64, -1.0_real64, 1)
-        E = E + E_adj
-
-        do i = 1,electrons
-            call normalise(psi(:, i))
-        end do
-
-        do i = 1, hartree_iterations
-            do j = 1, electrons
-                rho = 0.0_real64
-                do k = 1, electrons
-                    if (j /= k) rho = rho + abs(psi(:, k))**2
-                end do
-
-                Qenc = 0.0_real64
-                do k = 2, N
-                    Qenc(k) = simpsons(rho(:k), k)
-                end do
-
-                V = -Z/r + Qenc/r
-
-                call numerov_cooley(psi(:, j), E(j), V)
-                call normalise(psi(:, j))
-
-                write (stderr, *) &
-                    "Hartree Iteration", i, &
-                    "Electron", j, &
-                    "E", E(j)
-            end do
-        end do
+        call hartree_method_s
     end subroutine
 end module hartree
diff --git a/src/hartree_s.f90 b/src/hartree_s.f90
new file mode 100644
index 0000000..7a1a17e
--- /dev/null
+++ b/src/hartree_s.f90
@@ -0,0 +1,58 @@
+module hartree_s
+    use iso_fortran_env, only: stderr => error_unit, int32, real64
+    use cli, only: &
+        electrons, &
+        dr, E, E_adj, &
+        psi, &
+        N, Z
+    use integration, only: normalise, simpsons
+    use numerovcooley, only: numerov_search, numerov_cooley
+
+    implicit none
+
+    public :: hartree_method_s
+contains
+    subroutine hartree_method_s
+        integer(int32), parameter :: hartree_iterations = 5
+        real(real64) :: r(N), r_min, V(N), Qenc(N), rho(N)
+        integer(int32) :: i, j, k
+
+        r_min = 1.0e-5_real64
+        r = [(r_min + dr*i, i=0,N-1)]
+        psi = 0.0_real64
+        V = -Z/r
+
+        call numerov_search(psi(:, 1), E(1), V, -10.0_real64, -1.0_real64, 0)
+        call numerov_search(psi(:, 2), E(2), V, -10.0_real64, -1.0_real64, 0)
+        call numerov_search(psi(:, 3), E(3), V, -10.0_real64, -1.0_real64, 1)
+        E = E + E_adj
+
+        do i = 1,electrons
+            call normalise(psi(:, i))
+        end do
+
+        do i = 1, hartree_iterations
+            do j = 1, electrons
+                rho = 0.0_real64
+                do k = 1, electrons
+                    if (j /= k) rho = rho + abs(psi(:, k))**2
+                end do
+
+                Qenc = 0.0_real64
+                do k = 2, N
+                    Qenc(k) = simpsons(rho(:k), k)
+                end do
+
+                V = -Z/r + Qenc/r
+
+                call numerov_cooley(psi(:, j), E(j), V)
+                call normalise(psi(:, j))
+
+                write (stderr, *) &
+                    "Hartree Iteration", i, &
+                    "Electron", j, &
+                    "E", E(j)
+            end do
+        end do
+    end subroutine
+end module hartree_s
\ No newline at end of file
~~~

</details>



# [f8d041c](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/f8d041c48527bf3d788063adfcf7df094be2beaf): Shield `hartree_method` Step 2

![Call graph of step 2 of shielding hartree_method.](call-graphs/f8d041c.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/CMakeLists.txt b/src/CMakeLists.txt
index 8ff14de..5a3759d 100644
--- a/src/CMakeLists.txt
+++ b/src/CMakeLists.txt
@@ -3,9 +3,7 @@ set(
     cli.f90
     hartree.f90
     hartree_s.f90
-    integration.f90
     integration_s.f90
-    numerovcooley.f90
     numerovcooley_s.f90
     CACHE INTERNAL ""
 )
diff --git a/src/hartree.f90 b/src/hartree.f90
index 4ae8291..35049d2 100644
--- a/src/hartree.f90
+++ b/src/hartree.f90
@@ -19,6 +19,6 @@ contains
     end subroutine
 
     subroutine hartree_method
-        call hartree_method_s
+        call hartree_method_s(psi, E, dr, Z, N, E_adj)
     end subroutine
 end module hartree
diff --git a/src/hartree_s.f90 b/src/hartree_s.f90
index 7a1a17e..e092cb4 100644
--- a/src/hartree_s.f90
+++ b/src/hartree_s.f90
@@ -1,19 +1,20 @@
 module hartree_s
     use iso_fortran_env, only: stderr => error_unit, int32, real64
-    use cli, only: &
-        electrons, &
-        dr, E, E_adj, &
-        psi, &
-        N, Z
-    use integration, only: normalise, simpsons
-    use numerovcooley, only: numerov_search, numerov_cooley
+    use cli, only: electrons
+    use integration_s, only: normalise_s, simpsons_s
+    use numerovcooley_s, only: numerov_search_s, numerov_cooley_s
 
     implicit none
 
     public :: hartree_method_s
 contains
-    subroutine hartree_method_s
+    subroutine hartree_method_s(psi, E, dr, Z, N, E_adj)
+        integer(int32), intent(in) :: N, Z
+        real(real64), intent(out) :: psi(N, electrons), E(electrons)
+        real(real64), intent(in) :: dr, E_adj(electrons)
+
         integer(int32), parameter :: hartree_iterations = 5
+
         real(real64) :: r(N), r_min, V(N), Qenc(N), rho(N)
         integer(int32) :: i, j, k
 
@@ -22,13 +23,13 @@ contains
         psi = 0.0_real64
         V = -Z/r
 
-        call numerov_search(psi(:, 1), E(1), V, -10.0_real64, -1.0_real64, 0)
-        call numerov_search(psi(:, 2), E(2), V, -10.0_real64, -1.0_real64, 0)
-        call numerov_search(psi(:, 3), E(3), V, -10.0_real64, -1.0_real64, 1)
+        call numerov_search_s(psi(:, 1), E(1), V, dr, N, -10.0_real64, -1.0_real64, 0)
+        call numerov_search_s(psi(:, 2), E(2), V, dr, N, -10.0_real64, -1.0_real64, 0)
+        call numerov_search_s(psi(:, 3), E(3), V, dr, N, -10.0_real64, -1.0_real64, 1)
         E = E + E_adj
 
         do i = 1,electrons
-            call normalise(psi(:, i))
+            call normalise_s(psi(:, i), dr, N)
         end do
 
         do i = 1, hartree_iterations
@@ -40,13 +41,13 @@ contains
 
                 Qenc = 0.0_real64
                 do k = 2, N
-                    Qenc(k) = simpsons(rho(:k), k)
+                    Qenc(k) = simpsons_s(rho(:k), dr, k)
                 end do
 
                 V = -Z/r + Qenc/r
 
-                call numerov_cooley(psi(:, j), E(j), V)
-                call normalise(psi(:, j))
+                call numerov_cooley_s(psi(:, j), E(j), V, dr, N)
+                call normalise_s(psi(:, j), dr, N)
 
                 write (stderr, *) &
                     "Hartree Iteration", i, &
diff --git a/src/integration.f90 b/src/integration.f90
deleted file mode 100644
index a1f3d19..0000000
--- a/src/integration.f90
+++ /dev/null
@@ -1,24 +0,0 @@
-module integration
-    use iso_fortran_env, only: int32, real64
-    use cli, only: dr, N
-    use integration_s, only: normalise_s, simpsons_s
-
-    implicit none
-
-    private
-    public :: normalise, simpsons
-contains
-    subroutine normalise(psi)
-        real(real64), intent(inout) :: psi(N)
-
-        call normalise_s(psi, dr, N)
-    end subroutine
-
-    function simpsons(array, N)
-        integer(int32), intent(in) :: N
-        real(real64), intent(in) :: array(N)
-        real(real64) :: simpsons
-
-        simpsons = simpsons_s(array, dr, N)
-    end function
-end module integration
\ No newline at end of file
diff --git a/src/numerovcooley.f90 b/src/numerovcooley.f90
deleted file mode 100644
index 7373dbf..0000000
--- a/src/numerovcooley.f90
+++ /dev/null
@@ -1,31 +0,0 @@
-module numerovcooley
-    use iso_fortran_env, only: stderr => error_unit, int32, real64
-    use cli, only: dr, N
-    use numerovcooley_s, only: numerov_cooley_s, numerov_cooley_single_s, numerov_search_s
-
-    implicit none
-
-    private
-
-    public :: numerov_search, numerov_cooley
-contains
-    subroutine numerov_search(psi, E, V, lower, upper, target_nodes, max_iters)
-        integer(int32), intent(in) :: target_nodes
-        real(real64), intent(out) :: psi(N), E
-        real(real64), intent(in) :: V(N), lower, upper
-        integer(int32), intent(in), optional :: max_iters
-
-        integer(int32), parameter :: cooley_convergence_iters = 10000
-
-        call numerov_search_s(psi, E, V, dr, N, lower, upper, target_nodes, max_iters, cooley_convergence_iters)
-    end subroutine
-
-    subroutine numerov_cooley(psi, E, V, max_iters)
-        real(real64), intent(out) :: psi(N)
-        real(real64), intent(inout) :: E
-        real(real64), intent(in) :: V(N)
-        integer(int32), intent(in), optional :: max_iters
-
-        call numerov_cooley_s(psi, E, V, dr, N, max_iters)
-    end subroutine
-end module numerovcooley
~~~

</details>



# [33a9988](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/33a99889e631dcd896b08620be44459440bad8c4): Shield `hartree_method` Step 3

![Call graph of step 3 of shielding hartree_method.](call-graphs/33a9988.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/CMakeLists.txt b/src/CMakeLists.txt
index 5a3759d..cc7b019 100644
--- a/src/CMakeLists.txt
+++ b/src/CMakeLists.txt
@@ -3,8 +3,8 @@ set(
     cli.f90
     hartree.f90
     hartree_s.f90
-    integration_s.f90
-    numerovcooley_s.f90
+    integration.f90
+    numerovcooley.f90
     CACHE INTERNAL ""
 )
 
diff --git a/src/hartree_s.f90 b/src/hartree_s.f90
index e092cb4..1166812 100644
--- a/src/hartree_s.f90
+++ b/src/hartree_s.f90
@@ -1,8 +1,8 @@
 module hartree_s
     use iso_fortran_env, only: stderr => error_unit, int32, real64
     use cli, only: electrons
-    use integration_s, only: normalise_s, simpsons_s
-    use numerovcooley_s, only: numerov_search_s, numerov_cooley_s
+    use integration, only: normalise, simpsons
+    use numerovcooley, only: numerov_search, numerov_cooley
 
     implicit none
 
@@ -23,13 +23,13 @@ contains
         psi = 0.0_real64
         V = -Z/r
 
-        call numerov_search_s(psi(:, 1), E(1), V, dr, N, -10.0_real64, -1.0_real64, 0)
-        call numerov_search_s(psi(:, 2), E(2), V, dr, N, -10.0_real64, -1.0_real64, 0)
-        call numerov_search_s(psi(:, 3), E(3), V, dr, N, -10.0_real64, -1.0_real64, 1)
+        call numerov_search(psi(:, 1), E(1), V, dr, N, -10.0_real64, -1.0_real64, 0)
+        call numerov_search(psi(:, 2), E(2), V, dr, N, -10.0_real64, -1.0_real64, 0)
+        call numerov_search(psi(:, 3), E(3), V, dr, N, -10.0_real64, -1.0_real64, 1)
         E = E + E_adj
 
         do i = 1,electrons
-            call normalise_s(psi(:, i), dr, N)
+            call normalise(psi(:, i), dr, N)
         end do
 
         do i = 1, hartree_iterations
@@ -41,13 +41,13 @@ contains
 
                 Qenc = 0.0_real64
                 do k = 2, N
-                    Qenc(k) = simpsons_s(rho(:k), dr, k)
+                    Qenc(k) = simpsons(rho(:k), dr, k)
                 end do
 
                 V = -Z/r + Qenc/r
 
-                call numerov_cooley_s(psi(:, j), E(j), V, dr, N)
-                call normalise_s(psi(:, j), dr, N)
+                call numerov_cooley(psi(:, j), E(j), V, dr, N)
+                call normalise(psi(:, j), dr, N)
 
                 write (stderr, *) &
                     "Hartree Iteration", i, &
diff --git a/src/integration_s.f90 b/src/integration.f90
similarity index 82%
rename from src/integration_s.f90
rename to src/integration.f90
index 6f18ad4..0a3b11b 100644
--- a/src/integration_s.f90
+++ b/src/integration.f90
@@ -1,22 +1,22 @@
-module integration_s
+module integration
     use iso_fortran_env, only: int32, real64
 
     implicit none
 
-    public :: normalise_s, simpsons_s
+    public :: normalise, simpsons
 contains
-    subroutine normalise_s(psi, dr, N)
+    subroutine normalise(psi, dr, N)
         integer(int32), intent(in) :: N
         real(real64), intent(inout) :: psi(N)
         real(real64), intent(in) :: dr
 
         real(real64) :: area
 
-        area = simpsons_s(psi**2, dr, N)
+        area = simpsons(psi**2, dr, N)
         psi = psi / sqrt(area)
     end subroutine
 
-    function simpsons_s(array, dr, N) result(simpsons)
+    function simpsons(array, dr, N)
         integer(int32), intent(in) :: N
         real(real64), intent(in) :: dr, array(N)
         real(real64) :: simpsons
@@ -39,4 +39,4 @@ contains
             simpsons = simpsons + (array(N - 1) + array(N)) * dr / 2.0_real64
         end if
     end function
-end module integration_s
\ No newline at end of file
+end module integration
\ No newline at end of file
diff --git a/src/numerovcooley_s.f90 b/src/numerovcooley.f90
similarity index 88%
rename from src/numerovcooley_s.f90
rename to src/numerovcooley.f90
index b100d09..e0b909b 100644
--- a/src/numerovcooley_s.f90
+++ b/src/numerovcooley.f90
@@ -1,12 +1,12 @@
-module numerovcooley_s
+module numerovcooley
     use iso_fortran_env, only: stderr => error_unit, int32, real64
 
     implicit none
 
     private
-    public :: numerov_cooley_s, numerov_cooley_single_s, numerov_search_s
+    public :: numerov_cooley, numerov_cooley_single, numerov_search
 contains
-    subroutine numerov_search_s(psi, E, V, dr, N, lower, upper, target_nodes, max_iters, cooley_convergence_iters)
+    subroutine numerov_search(psi, E, V, dr, N, lower, upper, target_nodes, max_iters, cooley_convergence_iters)
         integer(int32), intent(in) :: N, target_nodes
         real(real64), intent(out) :: psi(N), E
         real(real64), intent(in) :: dr, V(N), lower, upper
@@ -27,7 +27,7 @@ contains
         do i = 1, max_iters_inner
             E = lower_inner + (upper_inner - lower_inner) / 2.0_real64
             prev_E = E
-            call numerov_cooley_single_s(psi, E, V, dr, N)
+            call numerov_cooley_single(psi, E, V, dr, N)
 
             nodes = 0
             prev_psi = psi(1)
@@ -62,10 +62,10 @@ contains
             end if
         end do
 
-        call numerov_cooley_s(psi, E, V, dr, N, cooley_convergence_iters)
+        call numerov_cooley(psi, E, V, dr, N, cooley_convergence_iters)
     end subroutine
 
-    subroutine numerov_cooley_s(psi, E, V, dr, N, max_iters)
+    subroutine numerov_cooley(psi, E, V, dr, N, max_iters)
         integer(int32), intent(in) :: N
         real(real64), intent(out) :: psi(N)
         real(real64), intent(inout) :: E
@@ -84,7 +84,7 @@ contains
 
         do i = 1, max_iters_inner
             prev_E = E
-            call numerov_cooley_single_s(psi, E, V, dr, N)
+            call numerov_cooley_single(psi, E, V, dr, N)
             dE = abs(prev_E - E)
 
             if (mod(i - 1, max_iters_inner / 10) == 0) then
@@ -101,7 +101,7 @@ contains
         end do
     end subroutine
 
-    subroutine numerov_cooley_single_s(psi, E, V, dr, N)
+    subroutine numerov_cooley_single(psi, E, V, dr, N)
         integer(int32), intent(in) :: N
         real(real64), intent(out) :: psi(N)
         real(real64), intent(inout) :: E
@@ -152,4 +152,4 @@ contains
 
         E = E + dE
     end subroutine
-end module numerovcooley_s
\ No newline at end of file
+end module numerovcooley
\ No newline at end of file
~~~

</details>

Now that most of the shielded procedures in the codebase no longer have equivalents in the adapter boundary, the `_s` suffix for them is unnecessary and has been removed.


# [03cb04c](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/03cb04c52b9da8577f2e6e99683f1d60f4fbd408): Shield `read_parameters` Step 1

![Call graph of step 1 of shielding read_parameters.](call-graphs/03cb04c.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/CMakeLists.txt b/src/CMakeLists.txt
index cc7b019..e6eb9fb 100644
--- a/src/CMakeLists.txt
+++ b/src/CMakeLists.txt
@@ -1,6 +1,7 @@
 set(
     MODULES
     cli.f90
+    cli_s.f90
     hartree.f90
     hartree_s.f90
     integration.f90
diff --git a/src/cli.f90 b/src/cli.f90
index 8881e11..9f2f000 100644
--- a/src/cli.f90
+++ b/src/cli.f90
@@ -1,112 +1,18 @@
 module cli
     use iso_fortran_env, only: stderr => error_unit, int32, real64
+    use cli_s, only: electrons, read_parameters_s, &
+        dr, r_max, Z, N, E, psi, E_adj
 
     implicit none
 
     private
 
-    integer(int32), parameter :: electrons = 3
-
-    real(real64) :: dr, E(electrons), E_adj(electrons), r_max
-    real(real64), allocatable :: psi(:, :)
-    integer(int32) :: N, Z
-
-    public :: read_parameters
-    public :: electrons
     public :: dr, E, E_adj, r_max
     public :: psi
     public :: N, Z
+    public :: electrons, read_parameters
 contains
     subroutine read_parameters
-        integer(int32) :: i, n
-        character(len=:), allocatable :: arg
-
-        Z = 3
-        E_adj = [0.0_real64, 0.0_real64, 0.875_real64]
-        r_max = 20.0_real64
-        dr = 1.0e-3_real64
-
-        n = command_argument_count()
-        i = 0
-        do while (i < n)
-            i = i + 1
-
-            arg = get_argument(i)
-
-            select case (arg)
-            case ("-h", "--help")
-                deallocate (arg)
-                arg = get_argument(0)
-                print *, &
-                    "usage: ", arg, &
-                    " [-r R-MAX]", &
-                    " [-Z CHARGE]", &
-                    " [-E ELECTRON-1-ENERGY-ADJUST ELECTRON-2-ENERGY-ADJUST ELECTRON-3-ENERGY-ADJUST]"
-                stop
-            case ("-Z", "--charge")
-                i = i + 1
-                if (i > n) then
-                    error stop "--charge (-Z) requires an argument"
-                end if
-                Z = get_integer_argument(i)
-            case ("-r", "--r-max")
-                i = i + 1
-                if (i > n) then
-                    error stop "--r-max (-r) requires an argument"
-                end if
-                r_max = get_real_argument(i)
-            case ("-d", "--r-step")
-                i = i + 1
-                if (i > n) then
-                    error stop "--r-step (-d) requires an argument"
-                end if
-                dr = get_real_argument(i)
-            case ("-E", "--energy-adjust")
-                if (i + 3 > n) then
-                    error stop "--energy-adjust (-E) requires 3 arguments"
-                end if
-
-                i = i + 1
-                E_adj(1) = get_real_argument(i)
-                i = i + 1
-                E_adj(2) = get_real_argument(i)
-                i = i + 1
-                E_adj(3) = get_real_argument(i)
-            case default
-                write (stderr, *) "Unknown argument: ", arg
-                error stop
-            end select
-
-            deallocate (arg)
-        end do
+        call read_parameters_s
     end subroutine
-
-    function get_integer_argument(i) result(argument)
-        integer(int32), intent(in) :: i
-        integer(int32) :: argument
-
-        character(len=:), allocatable :: text
-        text = get_argument(i)
-        read (text, *) argument
-    end function
-
-    function get_real_argument(i) result(argument)
-        integer(int32), intent(in) :: i
-        real(real64) :: argument
-
-        character(len=:), allocatable :: text
-        text = get_argument(i)
-        read (text, *) argument
-    end function
-
-    function get_argument(i) result(argument)
-        integer(int32), intent(in) :: i
-        character(len=:), allocatable :: argument
-
-        integer(int32) :: length
-
-        call get_command_argument(i, length = length)
-        allocate (character(len=length) :: argument)
-        call get_command_argument(i, value = argument)
-    end function
 end module cli
\ No newline at end of file
diff --git a/src/cli_s.f90 b/src/cli_s.f90
new file mode 100644
index 0000000..6f96fb8
--- /dev/null
+++ b/src/cli_s.f90
@@ -0,0 +1,109 @@
+module cli_s
+    use iso_fortran_env, only: stderr => error_unit, int32, real64
+
+    implicit none
+
+    integer(int32), parameter :: electrons = 3
+
+    real(real64) :: dr, E(electrons), E_adj(electrons), r_max
+    real(real64), allocatable :: psi(:, :)
+    integer(int32) :: N, Z
+
+    public :: dr, E, E_adj, r_max
+    public :: psi
+    public :: N, Z
+    public :: electrons, read_parameters_s
+contains
+    subroutine read_parameters_s
+        integer(int32) :: i, n
+        character(len=:), allocatable :: arg
+
+        Z = 3
+        E_adj = [0.0_real64, 0.0_real64, 0.875_real64]
+        r_max = 20.0_real64
+        dr = 1.0e-3_real64
+
+        n = command_argument_count()
+        i = 0
+        do while (i < n)
+            i = i + 1
+
+            arg = get_argument(i)
+
+            select case (arg)
+            case ("-h", "--help")
+                deallocate (arg)
+                arg = get_argument(0)
+                print *, &
+                    "usage: ", arg, &
+                    " [-r R-MAX]", &
+                    " [-Z CHARGE]", &
+                    " [-E ELECTRON-1-ENERGY-ADJUST ELECTRON-2-ENERGY-ADJUST ELECTRON-3-ENERGY-ADJUST]"
+                stop
+            case ("-Z", "--charge")
+                i = i + 1
+                if (i > n) then
+                    error stop "--charge (-Z) requires an argument"
+                end if
+                Z = get_integer_argument(i)
+            case ("-r", "--r-max")
+                i = i + 1
+                if (i > n) then
+                    error stop "--r-max (-r) requires an argument"
+                end if
+                r_max = get_real_argument(i)
+            case ("-d", "--r-step")
+                i = i + 1
+                if (i > n) then
+                    error stop "--r-step (-d) requires an argument"
+                end if
+                dr = get_real_argument(i)
+            case ("-E", "--energy-adjust")
+                if (i + 3 > n) then
+                    error stop "--energy-adjust (-E) requires 3 arguments"
+                end if
+
+                i = i + 1
+                E_adj(1) = get_real_argument(i)
+                i = i + 1
+                E_adj(2) = get_real_argument(i)
+                i = i + 1
+                E_adj(3) = get_real_argument(i)
+            case default
+                write (stderr, *) "Unknown argument: ", arg
+                error stop
+            end select
+
+            deallocate (arg)
+        end do
+    end subroutine
+
+    function get_integer_argument(i) result(argument)
+        integer(int32), intent(in) :: i
+        integer(int32) :: argument
+
+        character(len=:), allocatable :: text
+        text = get_argument(i)
+        read (text, *) argument
+    end function
+
+    function get_real_argument(i) result(argument)
+        integer(int32), intent(in) :: i
+        real(real64) :: argument
+
+        character(len=:), allocatable :: text
+        text = get_argument(i)
+        read (text, *) argument
+    end function
+
+    function get_argument(i) result(argument)
+        integer(int32), intent(in) :: i
+        character(len=:), allocatable :: argument
+
+        integer(int32) :: length
+
+        call get_command_argument(i, length = length)
+        allocate (character(len=length) :: argument)
+        call get_command_argument(i, value = argument)
+    end function
+end module cli_s
\ No newline at end of file
~~~

</details>



# [ab7468c](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/ab7468ccfae10509eaed767c312e60a0afdc19bb): Shield `read_parameters` Step 2

![Call graph of step 2 of shielding read_parameters.](call-graphs/ab7468c.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/cli.f90 b/src/cli.f90
index 9f2f000..e76bb91 100644
--- a/src/cli.f90
+++ b/src/cli.f90
@@ -1,18 +1,21 @@
 module cli
-    use iso_fortran_env, only: stderr => error_unit, int32, real64
-    use cli_s, only: electrons, read_parameters_s, &
-        dr, r_max, Z, N, E, psi, E_adj
+    use iso_fortran_env, only: int32, real64
+    use cli_s, only: electrons, read_parameters_s
 
     implicit none
 
     private
 
+    real(real64) :: dr, E(electrons), E_adj(electrons), r_max
+    real(real64), allocatable :: psi(:, :)
+    integer(int32) :: N, Z
+
+    public :: electrons, read_parameters
     public :: dr, E, E_adj, r_max
     public :: psi
     public :: N, Z
-    public :: electrons, read_parameters
 contains
     subroutine read_parameters
-        call read_parameters_s
+        call read_parameters_s(dr, r_max, Z, E_adj)
     end subroutine
 end module cli
\ No newline at end of file
diff --git a/src/cli_s.f90 b/src/cli_s.f90
index 6f96fb8..49787ce 100644
--- a/src/cli_s.f90
+++ b/src/cli_s.f90
@@ -5,16 +5,12 @@ module cli_s
 
     integer(int32), parameter :: electrons = 3
 
-    real(real64) :: dr, E(electrons), E_adj(electrons), r_max
-    real(real64), allocatable :: psi(:, :)
-    integer(int32) :: N, Z
-
-    public :: dr, E, E_adj, r_max
-    public :: psi
-    public :: N, Z
     public :: electrons, read_parameters_s
 contains
-    subroutine read_parameters_s
+    subroutine read_parameters_s(dr, r_max, Z, E_adj)
+        real(real64), intent(out) :: dr, r_max, E_adj(electrons)
+        integer(int32), intent(out) :: Z
+
         integer(int32) :: i, n
         character(len=:), allocatable :: arg
 
~~~

</details>



# [364bc07](https://gitlab.com/seamsay-phd/rsecon23-example/-/commit/364bc07009e599c22cdf4a63fc7fcbf0a1fa6bac): Shield `Entry Points` Step 1

![Call graph of step 1 of shielding Entry Points.](call-graphs/364bc07.svg)

<details>
<summary>Diff Of `src/`</summary>

~~~ diff
diff --git a/src/CMakeLists.txt b/src/CMakeLists.txt
index e6eb9fb..cda8feb 100644
--- a/src/CMakeLists.txt
+++ b/src/CMakeLists.txt
@@ -1,9 +1,7 @@
 set(
     MODULES
     cli.f90
-    cli_s.f90
     hartree.f90
-    hartree_s.f90
     integration.f90
     numerovcooley.f90
     CACHE INTERNAL ""
diff --git a/src/cli.f90 b/src/cli.f90
index e76bb91..33fa8ce 100644
--- a/src/cli.f90
+++ b/src/cli.f90
@@ -1,21 +1,105 @@
 module cli
-    use iso_fortran_env, only: int32, real64
-    use cli_s, only: electrons, read_parameters_s
+    use iso_fortran_env, only: stderr => error_unit, int32, real64
 
     implicit none
 
-    private
-
-    real(real64) :: dr, E(electrons), E_adj(electrons), r_max
-    real(real64), allocatable :: psi(:, :)
-    integer(int32) :: N, Z
+    integer(int32), parameter :: electrons = 3
 
     public :: electrons, read_parameters
-    public :: dr, E, E_adj, r_max
-    public :: psi
-    public :: N, Z
 contains
-    subroutine read_parameters
-        call read_parameters_s(dr, r_max, Z, E_adj)
+    subroutine read_parameters(dr, r_max, Z, E_adj)
+        real(real64), intent(out) :: dr, r_max, E_adj(electrons)
+        integer(int32), intent(out) :: Z
+
+        integer(int32) :: i, n
+        character(len=:), allocatable :: arg
+
+        Z = 3
+        E_adj = [0.0_real64, 0.0_real64, 0.875_real64]
+        r_max = 20.0_real64
+        dr = 1.0e-3_real64
+
+        n = command_argument_count()
+        i = 0
+        do while (i < n)
+            i = i + 1
+
+            arg = get_argument(i)
+
+            select case (arg)
+            case ("-h", "--help")
+                deallocate (arg)
+                arg = get_argument(0)
+                print *, &
+                    "usage: ", arg, &
+                    " [-r R-MAX]", &
+                    " [-Z CHARGE]", &
+                    " [-E ELECTRON-1-ENERGY-ADJUST ELECTRON-2-ENERGY-ADJUST ELECTRON-3-ENERGY-ADJUST]"
+                stop
+            case ("-Z", "--charge")
+                i = i + 1
+                if (i > n) then
+                    error stop "--charge (-Z) requires an argument"
+                end if
+                Z = get_integer_argument(i)
+            case ("-r", "--r-max")
+                i = i + 1
+                if (i > n) then
+                    error stop "--r-max (-r) requires an argument"
+                end if
+                r_max = get_real_argument(i)
+            case ("-d", "--r-step")
+                i = i + 1
+                if (i > n) then
+                    error stop "--r-step (-d) requires an argument"
+                end if
+                dr = get_real_argument(i)
+            case ("-E", "--energy-adjust")
+                if (i + 3 > n) then
+                    error stop "--energy-adjust (-E) requires 3 arguments"
+                end if
+
+                i = i + 1
+                E_adj(1) = get_real_argument(i)
+                i = i + 1
+                E_adj(2) = get_real_argument(i)
+                i = i + 1
+                E_adj(3) = get_real_argument(i)
+            case default
+                write (stderr, *) "Unknown argument: ", arg
+                error stop
+            end select
+
+            deallocate (arg)
+        end do
     end subroutine
+
+    function get_integer_argument(i) result(argument)
+        integer(int32), intent(in) :: i
+        integer(int32) :: argument
+
+        character(len=:), allocatable :: text
+        text = get_argument(i)
+        read (text, *) argument
+    end function
+
+    function get_real_argument(i) result(argument)
+        integer(int32), intent(in) :: i
+        real(real64) :: argument
+
+        character(len=:), allocatable :: text
+        text = get_argument(i)
+        read (text, *) argument
+    end function
+
+    function get_argument(i) result(argument)
+        integer(int32), intent(in) :: i
+        character(len=:), allocatable :: argument
+
+        integer(int32) :: length
+
+        call get_command_argument(i, length = length)
+        allocate (character(len=length) :: argument)
+        call get_command_argument(i, value = argument)
+    end function
 end module cli
\ No newline at end of file
diff --git a/src/cli_s.f90 b/src/cli_s.f90
deleted file mode 100644
index 49787ce..0000000
--- a/src/cli_s.f90
+++ /dev/null
@@ -1,105 +0,0 @@
-module cli_s
-    use iso_fortran_env, only: stderr => error_unit, int32, real64
-
-    implicit none
-
-    integer(int32), parameter :: electrons = 3
-
-    public :: electrons, read_parameters_s
-contains
-    subroutine read_parameters_s(dr, r_max, Z, E_adj)
-        real(real64), intent(out) :: dr, r_max, E_adj(electrons)
-        integer(int32), intent(out) :: Z
-
-        integer(int32) :: i, n
-        character(len=:), allocatable :: arg
-
-        Z = 3
-        E_adj = [0.0_real64, 0.0_real64, 0.875_real64]
-        r_max = 20.0_real64
-        dr = 1.0e-3_real64
-
-        n = command_argument_count()
-        i = 0
-        do while (i < n)
-            i = i + 1
-
-            arg = get_argument(i)
-
-            select case (arg)
-            case ("-h", "--help")
-                deallocate (arg)
-                arg = get_argument(0)
-                print *, &
-                    "usage: ", arg, &
-                    " [-r R-MAX]", &
-                    " [-Z CHARGE]", &
-                    " [-E ELECTRON-1-ENERGY-ADJUST ELECTRON-2-ENERGY-ADJUST ELECTRON-3-ENERGY-ADJUST]"
-                stop
-            case ("-Z", "--charge")
-                i = i + 1
-                if (i > n) then
-                    error stop "--charge (-Z) requires an argument"
-                end if
-                Z = get_integer_argument(i)
-            case ("-r", "--r-max")
-                i = i + 1
-                if (i > n) then
-                    error stop "--r-max (-r) requires an argument"
-                end if
-                r_max = get_real_argument(i)
-            case ("-d", "--r-step")
-                i = i + 1
-                if (i > n) then
-                    error stop "--r-step (-d) requires an argument"
-                end if
-                dr = get_real_argument(i)
-            case ("-E", "--energy-adjust")
-                if (i + 3 > n) then
-                    error stop "--energy-adjust (-E) requires 3 arguments"
-                end if
-
-                i = i + 1
-                E_adj(1) = get_real_argument(i)
-                i = i + 1
-                E_adj(2) = get_real_argument(i)
-                i = i + 1
-                E_adj(3) = get_real_argument(i)
-            case default
-                write (stderr, *) "Unknown argument: ", arg
-                error stop
-            end select
-
-            deallocate (arg)
-        end do
-    end subroutine
-
-    function get_integer_argument(i) result(argument)
-        integer(int32), intent(in) :: i
-        integer(int32) :: argument
-
-        character(len=:), allocatable :: text
-        text = get_argument(i)
-        read (text, *) argument
-    end function
-
-    function get_real_argument(i) result(argument)
-        integer(int32), intent(in) :: i
-        real(real64) :: argument
-
-        character(len=:), allocatable :: text
-        text = get_argument(i)
-        read (text, *) argument
-    end function
-
-    function get_argument(i) result(argument)
-        integer(int32), intent(in) :: i
-        character(len=:), allocatable :: argument
-
-        integer(int32) :: length
-
-        call get_command_argument(i, length = length)
-        allocate (character(len=length) :: argument)
-        call get_command_argument(i, value = argument)
-    end function
-end module cli_s
\ No newline at end of file
diff --git a/src/hartree.f90 b/src/hartree.f90
index 35049d2..404fdbe 100644
--- a/src/hartree.f90
+++ b/src/hartree.f90
@@ -1,24 +1,68 @@
 module hartree
     use iso_fortran_env, only: stderr => error_unit, int32, real64
-    use cli, only: &
-        electrons, &
-        dr, E, E_adj, &
-        psi, &
-        N, Z
-    use hartree_s, only: hartree_method_s
+    use cli, only: electrons
+    use integration, only: normalise, simpsons
+    use numerovcooley, only: numerov_search, numerov_cooley
 
     implicit none
 
     private
     public :: hartree_method, lithium
 contains
-    subroutine lithium
-        Z = 3
-        E_adj = [0.0_real64, 0.0_real64, 0.875_real64]
-        call hartree_method
+    subroutine lithium(psi, E, dr, N)
+        integer(int32), intent(in) :: N
+        real(real64), intent(out) :: psi(N, electrons), E(electrons)
+        real(real64), intent(in) :: dr
+
+        call hartree_method(psi, E, dr, 3, N, [0.0_real64, 0.0_real64, 0.875_real64])
     end subroutine
 
-    subroutine hartree_method
-        call hartree_method_s(psi, E, dr, Z, N, E_adj)
+    subroutine hartree_method(psi, E, dr, Z, N, E_adj)
+        integer(int32), intent(in) :: N, Z
+        real(real64), intent(out) :: psi(N, electrons), E(electrons)
+        real(real64), intent(in) :: dr, E_adj(electrons)
+
+        integer(int32), parameter :: hartree_iterations = 5
+
+        real(real64) :: r(N), r_min, V(N), Qenc(N), rho(N)
+        integer(int32) :: i, j, k
+
+        r_min = 1.0e-5_real64
+        r = [(r_min + dr*i, i=0,N-1)]
+        psi = 0.0_real64
+        V = -Z/r
+
+        call numerov_search(psi(:, 1), E(1), V, dr, N, -10.0_real64, -1.0_real64, 0)
+        call numerov_search(psi(:, 2), E(2), V, dr, N, -10.0_real64, -1.0_real64, 0)
+        call numerov_search(psi(:, 3), E(3), V, dr, N, -10.0_real64, -1.0_real64, 1)
+        E = E + E_adj
+
+        do i = 1,electrons
+            call normalise(psi(:, i), dr, N)
+        end do
+
+        do i = 1, hartree_iterations
+            do j = 1, electrons
+                rho = 0.0_real64
+                do k = 1, electrons
+                    if (j /= k) rho = rho + abs(psi(:, k))**2
+                end do
+
+                Qenc = 0.0_real64
+                do k = 2, N
+                    Qenc(k) = simpsons(rho(:k), dr, k)
+                end do
+
+                V = -Z/r + Qenc/r
+
+                call numerov_cooley(psi(:, j), E(j), V, dr, N)
+                call normalise(psi(:, j), dr, N)
+
+                write (stderr, *) &
+                    "Hartree Iteration", i, &
+                    "Electron", j, &
+                    "E", E(j)
+            end do
+        end do
     end subroutine
 end module hartree
diff --git a/src/hartree_s.f90 b/src/hartree_s.f90
deleted file mode 100644
index 1166812..0000000
--- a/src/hartree_s.f90
+++ /dev/null
@@ -1,59 +0,0 @@
-module hartree_s
-    use iso_fortran_env, only: stderr => error_unit, int32, real64
-    use cli, only: electrons
-    use integration, only: normalise, simpsons
-    use numerovcooley, only: numerov_search, numerov_cooley
-
-    implicit none
-
-    public :: hartree_method_s
-contains
-    subroutine hartree_method_s(psi, E, dr, Z, N, E_adj)
-        integer(int32), intent(in) :: N, Z
-        real(real64), intent(out) :: psi(N, electrons), E(electrons)
-        real(real64), intent(in) :: dr, E_adj(electrons)
-
-        integer(int32), parameter :: hartree_iterations = 5
-
-        real(real64) :: r(N), r_min, V(N), Qenc(N), rho(N)
-        integer(int32) :: i, j, k
-
-        r_min = 1.0e-5_real64
-        r = [(r_min + dr*i, i=0,N-1)]
-        psi = 0.0_real64
-        V = -Z/r
-
-        call numerov_search(psi(:, 1), E(1), V, dr, N, -10.0_real64, -1.0_real64, 0)
-        call numerov_search(psi(:, 2), E(2), V, dr, N, -10.0_real64, -1.0_real64, 0)
-        call numerov_search(psi(:, 3), E(3), V, dr, N, -10.0_real64, -1.0_real64, 1)
-        E = E + E_adj
-
-        do i = 1,electrons
-            call normalise(psi(:, i), dr, N)
-        end do
-
-        do i = 1, hartree_iterations
-            do j = 1, electrons
-                rho = 0.0_real64
-                do k = 1, electrons
-                    if (j /= k) rho = rho + abs(psi(:, k))**2
-                end do
-
-                Qenc = 0.0_real64
-                do k = 2, N
-                    Qenc(k) = simpsons(rho(:k), dr, k)
-                end do
-
-                V = -Z/r + Qenc/r
-
-                call numerov_cooley(psi(:, j), E(j), V, dr, N)
-                call normalise(psi(:, j), dr, N)
-
-                write (stderr, *) &
-                    "Hartree Iteration", i, &
-                    "Electron", j, &
-                    "E", E(j)
-            end do
-        end do
-    end subroutine
-end module hartree_s
\ No newline at end of file
~~~

</details>



