program numerov_search_test
    use iso_fortran_env, only: int32, real64
    use numerovcooley, only: numerov_search

    implicit none

    integer(int32), parameter :: num_tests = 2_int32, N = 20000_int32, Z = 3_int32
    real(real64), parameter :: dr = 1e-3_real64

    integer(int32) :: i, target_nodes(num_tests)
    real(real64) :: E, expected_E(num_tests), r(N), V(N), psi(N)
    logical :: success

    r = [(1.0e-5_real64 + dr*i, i=0,N-1)]
    V = -Z / r

    target_nodes = [0_int32, 1_int32]
    expected_E = [-4.513214_real64, -1.164729_real64]

    success = .true.
    !$omp parallel do private(i, psi, E)
    do i = 1,num_tests
        call numerov_search(psi, E, V, dr, N, -10.0_real64, -1.0_real64, target_nodes(i))

        !$omp critical
        if (abs(E - expected_E(i)) > 1.0e-6_real64) then
            print *, "Test", i, "Failed: E", E, "expected_E", expected_E(i)
            success = .false.
        end if
        !$omp end critical
    end do
    !$omp end parallel do

    if (.not. success) error stop
end program numerov_search_test