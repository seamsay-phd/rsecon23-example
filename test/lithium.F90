program lithium_test
    use iso_fortran_env, only: int32, real64
    use cli, only: electrons
    use hartree, only: lithium

    implicit none

    logical :: success
    character(len=:), allocatable :: data_file
    integer(int32) :: i, j, sep_idx, io, N
    real(real64) :: E(electrons), dr, expected_E(electrons)
    real(real64), allocatable :: psi(:, :), expected_psi(:, :)

    dr = 1.0e-3_real64
    N = 20000_int32
    allocate (psi(N, electrons), expected_psi(N, electrons))

    data_file = __FILE__

    sep_idx = index(data_file, "/", back = .true.)
    if (sep_idx < 1) error stop "Unknown error."
    if (data_file(sep_idx+1:) /= "lithium.F90") error stop "Sense-check failed, something about the test has changed."

    data_file = data_file(:sep_idx-1)//"/lithium.txt"

    open (newunit = io, file = data_file, status = "old", action = "read", form = "formatted")
    read (io, '(*(ES23.16,X))') expected_E
    do i = 1, electrons
        read (io, '(*(ES23.16,X))') expected_psi(:, i)
    end do

    call lithium(psi, E, dr, N)

    success = .true.
    do i = 1, electrons
        if (abs(E(i) - expected_E(i)) > 1.0e-6_real64) then
            write (*, "(A,X,I0,X,G13.6,X,A,X,G13.6,A)") &
                "Energy", i, "incorrect:", E(i), "(actual) /=", expected_E(i), "(expected)"

            success = .false.
        end if

        do j = 1, N
            if (abs(psi(j, i) - expected_psi(j, i)) > 1.0e-6_real64) then
                write (*, "(A,X,I0,X,A,X,I0,X,A,X,G13.6,X,A,X,G13.6,A)") &
                    "Element", j, "of electron", i, "wavefunction incorrect:", &
                    psi(j, i), "(actual) /=", expected_psi(j, i), "(expected)"

                success = .false.
            end if
        end do
    end do

    if (.not. success) error stop
end program lithium_test
