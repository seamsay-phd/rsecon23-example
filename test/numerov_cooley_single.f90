program numerov_cooley_single_test
    use iso_fortran_env, only: int32, real64
    use numerovcooley, only: numerov_cooley_single

    implicit none

    type test_data
        integer(int32) :: N
        real(real64) :: E, dr, expected_E
        real(real64), allocatable :: V(:), expected_psi(:)
    end type

    integer(int32), parameter :: num_tests = 2

    type(test_data) :: test, tests(num_tests)
    integer(int32) :: i, j
    real(real64), allocatable :: psi(:)
    logical :: success

    tests = [ &
        test_data( &
            N = 10_int32, &
            E = -4.5_real64, &
            dr = 1.0e-3_real64, &
            V = [ &
                9999.9_real64, &
                9999.9_real64, &
                9999.9_real64, &
                0.0_real64, &
                0.0_real64, &
                0.0_real64, &
                0.0_real64, &
                9999.9_real64, &
                9999.9_real64, &
                9999.9_real64 &
            ], &
            expected_E = 29837.413430_real64, &
            expected_psi = [&
                0.0_real64, &
                0.116850_real64, &
                0.236042_real64, &
                0.359964_real64, &
                0.485092_real64, &
                0.610224_real64, &
                0.735362_real64, &
                0.860506_real64, &
                1.0_real64, &
                0.0_real64 &
            ] &
        ), &
        test_data( &
            N = 11_int32, &
            E = -4.5_real64, &
            dr = 1.0_real64, &
            V = [ &
                25.0_real64, &
                16.0_real64, &
                9.0_real64, &
                4.0_real64, &
                1.0_real64, &
                0.0_real64, &
                1.0_real64, &
                4.0_real64, &
                9.0_real64, &
                16.0_real64, &
                25.0_real64 &
            ], &
            expected_E = 1.746596_real64, &
            expected_psi = [&
                0.0_real64, &
                0.010797_real64, &
                -0.099705_real64, &
                1.0_real64, &
                0.022392_real64, &
                0.000196_real64, &
                -0.000007_real64, &
                0.0_real64, &
                0.0_real64, &
                0.0_real64, &
                0.0_real64 &
            ] &
        ) &
    ]

    success = .true.
    !$omp parallel do private(i, psi, test)
    do i = 1,num_tests
        test = tests(i)
        allocate (psi(test%N))
        call numerov_cooley_single(psi, test%E, test%V, test%dr, test%N)

        !$omp critical
        if (abs(test%E - test%expected_E) > 1.0e-6_real64) then
            print *, "Test", i, "Failed: E", test%E, "expected_E", test%expected_E
            success = .false.
        end if

        do j = 1, test%N
            if (abs(psi(j) - test%expected_psi(j)) > 1.0e-6_real64) then
                print *, "Test", i, "Element", j, "Failed: E", psi(j), "expected_E", test%expected_psi(j)
                success = .false.
            end if
        end do
        !$omp end critical

        deallocate (psi)
    end do
    !$omp end parallel do

    if (.not. success) error stop
end program numerov_cooley_single_test