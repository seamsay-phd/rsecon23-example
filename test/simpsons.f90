program simpsons_test
    use iso_fortran_env, only: int32, real64
    use integration, only: simpsons

    implicit none

    type test_data
        integer(int32) :: N
        real(real64) :: dr, expected
        real(real64), allocatable :: array(:)
    end type

    type(test_data) :: test, tests(2)
    real(real64) :: actual
    integer(int32) :: i
    logical :: success

    tests = [ &
        test_data( &
            N = 3_int32, &
            dr = 1.0_real64, &
            array = [0.0_real64, 1.0_real64, 0.0_real64], &
            expected = 0.833333_real64 &
        ), &
        test_data( &
            N = 4_int32, &
            dr = 0.1_real64, &
            array = [0.0_real64, 1.0_real64, 1.0_real64, 0.0_real64], &
            expected = 0.133333_real64 &
        ) &
    ]

    success = .true.
    !$omp parallel do private(i, actual)
    do i = 1,2
        test = tests(i)
        actual = simpsons(test%array, test%dr, test%N)
        if (abs(test%expected - actual) > 1.0e-6_real64) then
            print *, "Test", i, "Failed: actual", actual, "expected", test%expected
            success = .false.
        end if
    end do
    !$omp end parallel do

    if (.not. success) error stop
end program simpsons_test