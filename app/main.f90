program main
    use iso_fortran_env, only: stderr => error_unit, int32, real64
    use cli, only: read_parameters, electrons
    use hartree, only: hartree_method

    implicit none

    real(real64), parameter :: Eh = 27.211386018_real64

    integer(int32) :: i, Z, N
    real(real64) :: dr, r_max, E(electrons), E_adj(electrons)
    real(real64), allocatable :: psi(:, :)

    call read_parameters(dr, r_max, Z, E_adj)
    write (stderr, *) "Parameters Parsed  ", "Z", Z, "R-Max", r_max, "R-Step", dr, "E", E_adj

    N = INT(r_max / dr, int32)
    write (stderr, *) "Derived Values  ", "N", N

    allocate (psi(N, electrons))

    call hartree_method(psi, E, dr, Z, N, E_adj)

    print *, E * Eh
    do i = 1, electrons
        print *, psi(:, i)
    end do
end program main
