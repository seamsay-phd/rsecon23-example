{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    fpm-flake.url = "gitlab:seamsay/fpm-flake";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };

  outputs = {
    self,
    flake-utils,
    fpm-flake,
    nixpkgs,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      fpm = fpm-flake.packages.${system}.fpm;
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      devShells.default = pkgs.mkShell {
        buildInputs = [fpm] ++ (with pkgs; [cmake d2 gfortran]);
      };

      formatter = pkgs.alejandra;
    });
}
